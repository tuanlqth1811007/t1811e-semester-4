package com.example.demoservice.dto;
import java.math.BigDecimal;
import java.util.Date;

public class ProductDTO {
    private String description;
    private  String name;
    private BigDecimal price;
    private Date createAt;
    private  Date updateAt;
}
