import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Table, Space, Button, Modal, Input, Divider } from "antd";
import "../assets/styles/listProductPage.css";
import axios from "axios";
import ColumnGroup from "antd/lib/table/ColumnGroup";
import { ExclamationCircleOutlined } from '@ant-design/icons';
import TextArea from "antd/lib/input/TextArea";

ListProductPage.propTypes = {};

const ACTION_TYPE = {
    POST: "POST",
    PUT: "PUT"
}
function ListProductPage(props) {
    const [data, setData] = useState([]);
    const [item, setItem] = useState({});
    const [isShowModalPostPut, setIsShowModalPostPut] = useState(false);
    const [actionType, setActionType] = useState("");
    const [errorName, setErrorName] = useState("");
    const [errorAddress, setErrorAddress] = useState("");

    const { confirm } = Modal;

    function showConfirmDelete(item) {
        confirm({
            title: 'Bạn có thật sự muốn xóa sản phẩm này?',
            icon: <ExclamationCircleOutlined />,
            content: item.name,
            onOk() {
                rqDeleteItem(item.id);
            },
            onCancel() {
                console.log('Cancel');
            },
        });
    }

    const columns = [
        {
            title: "Name",
            dataIndex: "name",
            key: "name",
        },
        {
            title: "Address",
            dataIndex: "address",
            key: "address",
        },
        {
            title: "Action",
            key: "action",
            render: (text, record) => (
                <Space size="middle">
                    <Button type="default" className="btn-edit" onClick={ () => { onClickEditItem(record) } }>Sửa</Button> <Button type="danger" danger className="btn-delete" onClick={ () => showConfirmDelete(record) }>Xóa</Button>
                </Space>
            ),
        },
    ];

    useEffect(() => {
        rqGetData();
    }, []);

    function formatDate(date) {
        let dateFormat = new Date(date);
        return formatDateNumber(dateFormat.getDate()) + "-" + formatDateNumber(dateFormat.getMonth() + 1) + "-" + dateFormat.getFullYear();
    }

    function formatDateNumber(number) {
        if (number < 10) return "0" + number;
        return number + "";
    }

    function onClickEditItem(item) {
        setItem(item);
        setActionType(ACTION_TYPE.PUT);
        setIsShowModalPostPut(true);
    }

    function rqGetData() {
        axios
            .get(`http://localhost:8080/api/v1/candidateDetails`)
            .then((res) => {
                setData(res.data);
            })
            .catch((error) => console.log(error));
    }

    function rqDeleteItem(id) {
        axios
            .delete(`http://localhost:8080/api/v1/candidateDetails/` + id)
            .then((res) => {
                rqGetData();
            })
            .catch((error) => console.log(error));
    }

    function rqPostItem() {
        axios
            .post(`http://localhost:8080/api/v1/candidateDetails`, JSON.stringify(item), {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then((res) => {
                rqGetData();
                setIsShowModalPostPut(false);

            })
            .catch((error) => showModalError());
    }

    function rqPutItem() {
        axios
            .post(`http://localhost:8080/api/v1/candidateDetails`, JSON.stringify(item), {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then((res) => {
                rqGetData();
                setIsShowModalPostPut(false);

            })
            .catch((error) => showModalError());
    }


    const onPostPutItem = function () {
        let hasError;
        if (item.name == "" || item.name == null) {
            setErrorName("Truờng này không được để trống");
            hasError = true;
        }
        if (item.address == "" || item.address == null) {
            setErrorAddress("Truờng này không được để trống");
            hasError = true;
        }
        if (!hasError) {
            if (actionType === ACTION_TYPE.POST) {
                rqPostItem();
            }
            if (actionType === ACTION_TYPE.PUT) {
                rqPutItem();
            }
        }
    }

    const handleCancelPostPutItem = function () {
        setErrorName(null);
        setErrorAddress(null);
        setIsShowModalPostPut(false);
    }

    function showModalError() {
        Modal.error({
            title: 'Thông báo',
            content: 'Đã xảy ra lỗi !',
        });
    }

    return (
        <div>
            <div className="list-product-page-header"><span style={ { fontWeight: "bold" } }>Candidate detail</span><span>Lê Quốc Tuấn - T1811E</span></div>
            <div className="list-product-page-body">
                <Button type="primary" primary onClick={ () => { setIsShowModalPostPut(true); setActionType(ACTION_TYPE.POST); setItem({}) } }>Thêm mới</Button>
                <Table columns={ columns } dataSource={ data } rowKey={ "id" } />
            </div>
            <Modal
                title={ actionType === ACTION_TYPE.PUT ? "Chỉnh sửa" : "Thêm mới" }
                visible={ isShowModalPostPut }
                onOk={ onPostPutItem }
                onCancel={ handleCancelPostPutItem }
            >
                <span className="label">Name</span>
                <Input autoFocus={ true } value={ item.name } onChange={ e => {
                    setItem({ ...item, name: e.target.value })
                } } />
                { errorName != null && errorName != "" && <span className="err-text" >{ "Không được để trống trường này" }</span> }
                <span className="label">Address</span>

                <Input
                    value={ item.address }
                    onChange={ e => {
                        setItem({ ...item, address: e.target.value })
                    } }
                />
                { errorName != null && errorName != "" && <span className="err-text">{ "Không được để trống trường này" }</span> }

            </Modal>
        </div>
    );
}

export default ListProductPage;
