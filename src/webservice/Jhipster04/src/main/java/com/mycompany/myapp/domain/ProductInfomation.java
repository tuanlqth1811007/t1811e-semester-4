package com.mycompany.myapp.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.UUID;

/**
 * A ProductInfomation.
 */
@Entity
@Table(name = "product_infomation")
public class ProductInfomation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "product_name")
    private String productName;

    @Column(name = "product_description")
    private String productDescription;

    @Column(name = "category_id")
    private UUID categoryId;

    @Column(name = "weight_class")
    private Integer weightClass;

    @Column(name = "warranty_period")
    private String warrantyPeriod;

    @Column(name = "supplied_id")
    private UUID suppliedID;

    @Column(name = "product_status")
    private Integer productStatus;

    @Column(name = "list_price")
    private String listPrice;

    @Column(name = "min_price")
    private Float minPrice;

    @Column(name = "catalog_url")
    private String catalogUrl;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public ProductInfomation productName(String productName) {
        this.productName = productName;
        return this;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public ProductInfomation productDescription(String productDescription) {
        this.productDescription = productDescription;
        return this;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public UUID getCategoryId() {
        return categoryId;
    }

    public ProductInfomation categoryId(UUID categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public void setCategoryId(UUID categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getWeightClass() {
        return weightClass;
    }

    public ProductInfomation weightClass(Integer weightClass) {
        this.weightClass = weightClass;
        return this;
    }

    public void setWeightClass(Integer weightClass) {
        this.weightClass = weightClass;
    }

    public String getWarrantyPeriod() {
        return warrantyPeriod;
    }

    public ProductInfomation warrantyPeriod(String warrantyPeriod) {
        this.warrantyPeriod = warrantyPeriod;
        return this;
    }

    public void setWarrantyPeriod(String warrantyPeriod) {
        this.warrantyPeriod = warrantyPeriod;
    }

    public UUID getSuppliedID() {
        return suppliedID;
    }

    public ProductInfomation suppliedID(UUID suppliedID) {
        this.suppliedID = suppliedID;
        return this;
    }

    public void setSuppliedID(UUID suppliedID) {
        this.suppliedID = suppliedID;
    }

    public Integer getProductStatus() {
        return productStatus;
    }

    public ProductInfomation productStatus(Integer productStatus) {
        this.productStatus = productStatus;
        return this;
    }

    public void setProductStatus(Integer productStatus) {
        this.productStatus = productStatus;
    }

    public String getListPrice() {
        return listPrice;
    }

    public ProductInfomation listPrice(String listPrice) {
        this.listPrice = listPrice;
        return this;
    }

    public void setListPrice(String listPrice) {
        this.listPrice = listPrice;
    }

    public Float getMinPrice() {
        return minPrice;
    }

    public ProductInfomation minPrice(Float minPrice) {
        this.minPrice = minPrice;
        return this;
    }

    public void setMinPrice(Float minPrice) {
        this.minPrice = minPrice;
    }

    public String getCatalogUrl() {
        return catalogUrl;
    }

    public ProductInfomation catalogUrl(String catalogUrl) {
        this.catalogUrl = catalogUrl;
        return this;
    }

    public void setCatalogUrl(String catalogUrl) {
        this.catalogUrl = catalogUrl;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductInfomation)) {
            return false;
        }
        return id != null && id.equals(((ProductInfomation) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProductInfomation{" +
            "id=" + getId() +
            ", productName='" + getProductName() + "'" +
            ", productDescription='" + getProductDescription() + "'" +
            ", categoryId='" + getCategoryId() + "'" +
            ", weightClass=" + getWeightClass() +
            ", warrantyPeriod='" + getWarrantyPeriod() + "'" +
            ", suppliedID='" + getSuppliedID() + "'" +
            ", productStatus=" + getProductStatus() +
            ", listPrice='" + getListPrice() + "'" +
            ", minPrice=" + getMinPrice() +
            ", catalogUrl='" + getCatalogUrl() + "'" +
            "}";
    }
}
