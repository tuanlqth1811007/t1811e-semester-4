package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.UUID;

/**
 * A Order.
 */
@Entity
@Table(name = "jhi_order")
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "order_date")
    private LocalDate orderDate;

    @Column(name = "order_mode")
    private String orderMode;

    @Column(name = "order_status")
    private Integer orderStatus;

    @Column(name = "order_total")
    private Integer orderTotal;

    @Column(name = "promotion_id")
    private UUID promotionId;

    @ManyToOne
    @JsonIgnoreProperties(value = "orders", allowSetters = true)
    private Employee salesRepId;

    @ManyToOne
    @JsonIgnoreProperties(value = "orders", allowSetters = true)
    private Customer customer;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public Order orderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
        return this;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderMode() {
        return orderMode;
    }

    public Order orderMode(String orderMode) {
        this.orderMode = orderMode;
        return this;
    }

    public void setOrderMode(String orderMode) {
        this.orderMode = orderMode;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public Order orderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
        return this;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Integer getOrderTotal() {
        return orderTotal;
    }

    public Order orderTotal(Integer orderTotal) {
        this.orderTotal = orderTotal;
        return this;
    }

    public void setOrderTotal(Integer orderTotal) {
        this.orderTotal = orderTotal;
    }

    public UUID getPromotionId() {
        return promotionId;
    }

    public Order promotionId(UUID promotionId) {
        this.promotionId = promotionId;
        return this;
    }

    public void setPromotionId(UUID promotionId) {
        this.promotionId = promotionId;
    }

    public Employee getSalesRepId() {
        return salesRepId;
    }

    public Order salesRepId(Employee employee) {
        this.salesRepId = employee;
        return this;
    }

    public void setSalesRepId(Employee employee) {
        this.salesRepId = employee;
    }

    public Customer getCustomer() {
        return customer;
    }

    public Order customer(Customer customer) {
        this.customer = customer;
        return this;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Order)) {
            return false;
        }
        return id != null && id.equals(((Order) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Order{" +
            "id=" + getId() +
            ", orderDate='" + getOrderDate() + "'" +
            ", orderMode='" + getOrderMode() + "'" +
            ", orderStatus=" + getOrderStatus() +
            ", orderTotal=" + getOrderTotal() +
            ", promotionId='" + getPromotionId() + "'" +
            "}";
    }
}
