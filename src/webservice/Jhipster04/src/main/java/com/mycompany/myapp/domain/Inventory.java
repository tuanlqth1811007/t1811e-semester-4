package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Inventory.
 */
@Entity
@Table(name = "inventory")
public class Inventory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "quantity_of_hand")
    private Integer quantityOfHand;

    @ManyToOne
    @JsonIgnoreProperties(value = "inventories", allowSetters = true)
    private WareHouse wareHouse;

    @ManyToOne
    @JsonIgnoreProperties(value = "inventories", allowSetters = true)
    private ProductInfomation product;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQuantityOfHand() {
        return quantityOfHand;
    }

    public Inventory quantityOfHand(Integer quantityOfHand) {
        this.quantityOfHand = quantityOfHand;
        return this;
    }

    public void setQuantityOfHand(Integer quantityOfHand) {
        this.quantityOfHand = quantityOfHand;
    }

    public WareHouse getWareHouse() {
        return wareHouse;
    }

    public Inventory wareHouse(WareHouse wareHouse) {
        this.wareHouse = wareHouse;
        return this;
    }

    public void setWareHouse(WareHouse wareHouse) {
        this.wareHouse = wareHouse;
    }

    public ProductInfomation getProduct() {
        return product;
    }

    public Inventory product(ProductInfomation productInfomation) {
        this.product = productInfomation;
        return this;
    }

    public void setProduct(ProductInfomation productInfomation) {
        this.product = productInfomation;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Inventory)) {
            return false;
        }
        return id != null && id.equals(((Inventory) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Inventory{" +
            "id=" + getId() +
            ", quantityOfHand=" + getQuantityOfHand() +
            "}";
    }
}
