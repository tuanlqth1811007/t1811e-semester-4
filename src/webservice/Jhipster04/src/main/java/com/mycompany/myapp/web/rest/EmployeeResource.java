package com.mycompany.myapp.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * EmployeeResource controller
 */
@RestController
@RequestMapping("/api/employee")
public class EmployeeResource {

    private final Logger log = LoggerFactory.getLogger(EmployeeResource.class);

    /**
    * POST createEmployee
    */
    @PostMapping("/create-employee")
    public String createEmployee() {
        return "createEmployee";
    }

    /**
    * GET getAllEmployee
    */
    @GetMapping("/get-all-employee")
    public String getAllEmployee() {
        return "getAllEmployee";
    }

    /**
    * GET findEmployee
    */
    @GetMapping("/find-employee")
    public String findEmployee() {
        return "findEmployee";
    }

}
