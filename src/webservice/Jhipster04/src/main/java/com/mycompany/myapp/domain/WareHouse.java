package com.mycompany.myapp.domain;


import javax.persistence.*;

import java.io.Serializable;

/**
 * A WareHouse.
 */
@Entity
@Table(name = "ware_house")
public class WareHouse implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "ware_house_spec")
    private String wareHouseSpec;

    @Column(name = "ware_house_name")
    private String wareHouseName;

    @Column(name = "wh_geo_location")
    private String whGeoLocation;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWareHouseSpec() {
        return wareHouseSpec;
    }

    public WareHouse wareHouseSpec(String wareHouseSpec) {
        this.wareHouseSpec = wareHouseSpec;
        return this;
    }

    public void setWareHouseSpec(String wareHouseSpec) {
        this.wareHouseSpec = wareHouseSpec;
    }

    public String getWareHouseName() {
        return wareHouseName;
    }

    public WareHouse wareHouseName(String wareHouseName) {
        this.wareHouseName = wareHouseName;
        return this;
    }

    public void setWareHouseName(String wareHouseName) {
        this.wareHouseName = wareHouseName;
    }

    public String getWhGeoLocation() {
        return whGeoLocation;
    }

    public WareHouse whGeoLocation(String whGeoLocation) {
        this.whGeoLocation = whGeoLocation;
        return this;
    }

    public void setWhGeoLocation(String whGeoLocation) {
        this.whGeoLocation = whGeoLocation;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof WareHouse)) {
            return false;
        }
        return id != null && id.equals(((WareHouse) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "WareHouse{" +
            "id=" + getId() +
            ", wareHouseSpec='" + getWareHouseSpec() + "'" +
            ", wareHouseName='" + getWareHouseName() + "'" +
            ", whGeoLocation='" + getWhGeoLocation() + "'" +
            "}";
    }
}
