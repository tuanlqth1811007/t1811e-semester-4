package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.WareHouse;
import com.mycompany.myapp.repository.WareHouseRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.WareHouse}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class WareHouseResource {

    private final Logger log = LoggerFactory.getLogger(WareHouseResource.class);

    private static final String ENTITY_NAME = "wareHouse";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final WareHouseRepository wareHouseRepository;

    public WareHouseResource(WareHouseRepository wareHouseRepository) {
        this.wareHouseRepository = wareHouseRepository;
    }

    /**
     * {@code POST  /ware-houses} : Create a new wareHouse.
     *
     * @param wareHouse the wareHouse to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new wareHouse, or with status {@code 400 (Bad Request)} if the wareHouse has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ware-houses")
    public ResponseEntity<WareHouse> createWareHouse(@RequestBody WareHouse wareHouse) throws URISyntaxException {
        log.debug("REST request to save WareHouse : {}", wareHouse);
        if (wareHouse.getId() != null) {
            throw new BadRequestAlertException("A new wareHouse cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WareHouse result = wareHouseRepository.save(wareHouse);
        return ResponseEntity.created(new URI("/api/ware-houses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ware-houses} : Updates an existing wareHouse.
     *
     * @param wareHouse the wareHouse to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated wareHouse,
     * or with status {@code 400 (Bad Request)} if the wareHouse is not valid,
     * or with status {@code 500 (Internal Server Error)} if the wareHouse couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ware-houses")
    public ResponseEntity<WareHouse> updateWareHouse(@RequestBody WareHouse wareHouse) throws URISyntaxException {
        log.debug("REST request to update WareHouse : {}", wareHouse);
        if (wareHouse.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WareHouse result = wareHouseRepository.save(wareHouse);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, wareHouse.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /ware-houses} : get all the wareHouses.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of wareHouses in body.
     */
    @GetMapping("/ware-houses")
    public List<WareHouse> getAllWareHouses() {
        log.debug("REST request to get all WareHouses");
        return wareHouseRepository.findAll();
    }

    /**
     * {@code GET  /ware-houses/:id} : get the "id" wareHouse.
     *
     * @param id the id of the wareHouse to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the wareHouse, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ware-houses/{id}")
    public ResponseEntity<WareHouse> getWareHouse(@PathVariable Long id) {
        log.debug("REST request to get WareHouse : {}", id);
        Optional<WareHouse> wareHouse = wareHouseRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(wareHouse);
    }

    /**
     * {@code DELETE  /ware-houses/:id} : delete the "id" wareHouse.
     *
     * @param id the id of the wareHouse to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ware-houses/{id}")
    public ResponseEntity<Void> deleteWareHouse(@PathVariable Long id) {
        log.debug("REST request to delete WareHouse : {}", id);
        wareHouseRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
