package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.ProductInfomation;
import com.mycompany.myapp.repository.ProductInfomationRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.ProductInfomation}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ProductInfomationResource {

    private final Logger log = LoggerFactory.getLogger(ProductInfomationResource.class);

    private static final String ENTITY_NAME = "productInfomation";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProductInfomationRepository productInfomationRepository;

    public ProductInfomationResource(ProductInfomationRepository productInfomationRepository) {
        this.productInfomationRepository = productInfomationRepository;
    }

    /**
     * {@code POST  /product-infomations} : Create a new productInfomation.
     *
     * @param productInfomation the productInfomation to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new productInfomation, or with status {@code 400 (Bad Request)} if the productInfomation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/product-infomations")
    public ResponseEntity<ProductInfomation> createProductInfomation(@RequestBody ProductInfomation productInfomation) throws URISyntaxException {
        log.debug("REST request to save ProductInfomation : {}", productInfomation);
        if (productInfomation.getId() != null) {
            throw new BadRequestAlertException("A new productInfomation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductInfomation result = productInfomationRepository.save(productInfomation);
        return ResponseEntity.created(new URI("/api/product-infomations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /product-infomations} : Updates an existing productInfomation.
     *
     * @param productInfomation the productInfomation to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated productInfomation,
     * or with status {@code 400 (Bad Request)} if the productInfomation is not valid,
     * or with status {@code 500 (Internal Server Error)} if the productInfomation couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/product-infomations")
    public ResponseEntity<ProductInfomation> updateProductInfomation(@RequestBody ProductInfomation productInfomation) throws URISyntaxException {
        log.debug("REST request to update ProductInfomation : {}", productInfomation);
        if (productInfomation.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProductInfomation result = productInfomationRepository.save(productInfomation);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, productInfomation.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /product-infomations} : get all the productInfomations.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of productInfomations in body.
     */
    @GetMapping("/product-infomations")
    public List<ProductInfomation> getAllProductInfomations() {
        log.debug("REST request to get all ProductInfomations");
        return productInfomationRepository.findAll();
    }

    /**
     * {@code GET  /product-infomations/:id} : get the "id" productInfomation.
     *
     * @param id the id of the productInfomation to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the productInfomation, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/product-infomations/{id}")
    public ResponseEntity<ProductInfomation> getProductInfomation(@PathVariable Long id) {
        log.debug("REST request to get ProductInfomation : {}", id);
        Optional<ProductInfomation> productInfomation = productInfomationRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(productInfomation);
    }

    /**
     * {@code DELETE  /product-infomations/:id} : delete the "id" productInfomation.
     *
     * @param id the id of the productInfomation to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/product-infomations/{id}")
    public ResponseEntity<Void> deleteProductInfomation(@PathVariable Long id) {
        log.debug("REST request to delete ProductInfomation : {}", id);
        productInfomationRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
