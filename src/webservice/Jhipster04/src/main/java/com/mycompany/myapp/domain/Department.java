package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.UUID;

/**
 * A Department.
 */
@Entity
@Table(name = "department")
public class Department implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "department_name")
    private String departmentName;

    @Column(name = "manager_id")
    private UUID managerId;

    @ManyToOne
    @JsonIgnoreProperties(value = "departments", allowSetters = true)
    private Location location;

    @ManyToOne
    @JsonIgnoreProperties(value = "departments", allowSetters = true)
    private WareHouse wareHouse;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public Department departmentName(String departmentName) {
        this.departmentName = departmentName;
        return this;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public UUID getManagerId() {
        return managerId;
    }

    public Department managerId(UUID managerId) {
        this.managerId = managerId;
        return this;
    }

    public void setManagerId(UUID managerId) {
        this.managerId = managerId;
    }

    public Location getLocation() {
        return location;
    }

    public Department location(Location location) {
        this.location = location;
        return this;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public WareHouse getWareHouse() {
        return wareHouse;
    }

    public Department wareHouse(WareHouse wareHouse) {
        this.wareHouse = wareHouse;
        return this;
    }

    public void setWareHouse(WareHouse wareHouse) {
        this.wareHouse = wareHouse;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Department)) {
            return false;
        }
        return id != null && id.equals(((Department) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Department{" +
            "id=" + getId() +
            ", departmentName='" + getDepartmentName() + "'" +
            ", managerId='" + getManagerId() + "'" +
            "}";
    }
}
