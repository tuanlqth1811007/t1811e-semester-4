package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A Customer.
 */
@Entity
@Table(name = "customer")
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "cust_first_name")
    private String custFirstName;

    @Column(name = "cust_last_name")
    private String custLastName;

    @Column(name = "cust_address")
    private String custAddress;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "n_lsanguage")
    private String nLsanguage;

    @Column(name = "nls_territory")
    private String nlsTerritory;

    @Column(name = "credit_limit")
    private Float creditLimit;

    @Column(name = "cust_email")
    private String custEmail;

    @Column(name = "account_mgr_id")
    private String accountMgrId;

    @Column(name = "cust_geo_location")
    private String custGeoLocation;

    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;

    @Column(name = "marital_status")
    private Integer maritalStatus;

    @Column(name = "gender")
    private Integer gender;

    @Column(name = "income_level")
    private Integer incomeLevel;

    @ManyToOne
    @JsonIgnoreProperties(value = "customers", allowSetters = true)
    private Employee employee;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustFirstName() {
        return custFirstName;
    }

    public Customer custFirstName(String custFirstName) {
        this.custFirstName = custFirstName;
        return this;
    }

    public void setCustFirstName(String custFirstName) {
        this.custFirstName = custFirstName;
    }

    public String getCustLastName() {
        return custLastName;
    }

    public Customer custLastName(String custLastName) {
        this.custLastName = custLastName;
        return this;
    }

    public void setCustLastName(String custLastName) {
        this.custLastName = custLastName;
    }

    public String getCustAddress() {
        return custAddress;
    }

    public Customer custAddress(String custAddress) {
        this.custAddress = custAddress;
        return this;
    }

    public void setCustAddress(String custAddress) {
        this.custAddress = custAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Customer phoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getnLsanguage() {
        return nLsanguage;
    }

    public Customer nLsanguage(String nLsanguage) {
        this.nLsanguage = nLsanguage;
        return this;
    }

    public void setnLsanguage(String nLsanguage) {
        this.nLsanguage = nLsanguage;
    }

    public String getNlsTerritory() {
        return nlsTerritory;
    }

    public Customer nlsTerritory(String nlsTerritory) {
        this.nlsTerritory = nlsTerritory;
        return this;
    }

    public void setNlsTerritory(String nlsTerritory) {
        this.nlsTerritory = nlsTerritory;
    }

    public Float getCreditLimit() {
        return creditLimit;
    }

    public Customer creditLimit(Float creditLimit) {
        this.creditLimit = creditLimit;
        return this;
    }

    public void setCreditLimit(Float creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getCustEmail() {
        return custEmail;
    }

    public Customer custEmail(String custEmail) {
        this.custEmail = custEmail;
        return this;
    }

    public void setCustEmail(String custEmail) {
        this.custEmail = custEmail;
    }

    public String getAccountMgrId() {
        return accountMgrId;
    }

    public Customer accountMgrId(String accountMgrId) {
        this.accountMgrId = accountMgrId;
        return this;
    }

    public void setAccountMgrId(String accountMgrId) {
        this.accountMgrId = accountMgrId;
    }

    public String getCustGeoLocation() {
        return custGeoLocation;
    }

    public Customer custGeoLocation(String custGeoLocation) {
        this.custGeoLocation = custGeoLocation;
        return this;
    }

    public void setCustGeoLocation(String custGeoLocation) {
        this.custGeoLocation = custGeoLocation;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public Customer dateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
        return this;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Integer getMaritalStatus() {
        return maritalStatus;
    }

    public Customer maritalStatus(Integer maritalStatus) {
        this.maritalStatus = maritalStatus;
        return this;
    }

    public void setMaritalStatus(Integer maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public Integer getGender() {
        return gender;
    }

    public Customer gender(Integer gender) {
        this.gender = gender;
        return this;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Integer getIncomeLevel() {
        return incomeLevel;
    }

    public Customer incomeLevel(Integer incomeLevel) {
        this.incomeLevel = incomeLevel;
        return this;
    }

    public void setIncomeLevel(Integer incomeLevel) {
        this.incomeLevel = incomeLevel;
    }

    public Employee getEmployee() {
        return employee;
    }

    public Customer employee(Employee employee) {
        this.employee = employee;
        return this;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Customer)) {
            return false;
        }
        return id != null && id.equals(((Customer) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Customer{" +
            "id=" + getId() +
            ", custFirstName='" + getCustFirstName() + "'" +
            ", custLastName='" + getCustLastName() + "'" +
            ", custAddress='" + getCustAddress() + "'" +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            ", nLsanguage='" + getnLsanguage() + "'" +
            ", nlsTerritory='" + getNlsTerritory() + "'" +
            ", creditLimit=" + getCreditLimit() +
            ", custEmail='" + getCustEmail() + "'" +
            ", accountMgrId='" + getAccountMgrId() + "'" +
            ", custGeoLocation='" + getCustGeoLocation() + "'" +
            ", dateOfBirth='" + getDateOfBirth() + "'" +
            ", maritalStatus=" + getMaritalStatus() +
            ", gender=" + getGender() +
            ", incomeLevel=" + getIncomeLevel() +
            "}";
    }
}
