package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.ProductInfomation;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ProductInfomation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductInfomationRepository extends JpaRepository<ProductInfomation, Long> {
}
