import { Moment } from 'moment';
import { IEmployee } from 'app/shared/model/employee.model';

export interface ICustomer {
  id?: number;
  custFirstName?: string;
  custLastName?: string;
  custAddress?: string;
  phoneNumber?: string;
  nLsanguage?: string;
  nlsTerritory?: string;
  creditLimit?: number;
  custEmail?: string;
  accountMgrId?: string;
  custGeoLocation?: string;
  dateOfBirth?: string;
  maritalStatus?: number;
  gender?: number;
  incomeLevel?: number;
  employee?: IEmployee;
}

export const defaultValue: Readonly<ICustomer> = {};
