import { Moment } from 'moment';
import { IJobHistory } from 'app/shared/model/job-history.model';
import { IJob } from 'app/shared/model/job.model';
import { IDepartment } from 'app/shared/model/department.model';

export interface IEmployee {
  id?: number;
  firstName?: string;
  lastName?: string;
  email?: string;
  phoneNumber?: string;
  hireDate?: string;
  salary?: number;
  commissionPct?: string;
  managerId?: string;
  jobHistory?: IJobHistory;
  job?: IJob;
  department?: IDepartment;
}

export const defaultValue: Readonly<IEmployee> = {};
