import { ILocation } from 'app/shared/model/location.model';
import { IWareHouse } from 'app/shared/model/ware-house.model';

export interface IDepartment {
  id?: number;
  departmentName?: string;
  managerId?: string;
  location?: ILocation;
  wareHouse?: IWareHouse;
}

export const defaultValue: Readonly<IDepartment> = {};
