import { IWareHouse } from 'app/shared/model/ware-house.model';
import { IProductInfomation } from 'app/shared/model/product-infomation.model';

export interface IInventory {
  id?: number;
  quantityOfHand?: number;
  wareHouse?: IWareHouse;
  product?: IProductInfomation;
}

export const defaultValue: Readonly<IInventory> = {};
