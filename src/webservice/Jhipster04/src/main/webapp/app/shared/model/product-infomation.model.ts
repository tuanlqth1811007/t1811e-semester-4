export interface IProductInfomation {
  id?: number;
  productName?: string;
  productDescription?: string;
  categoryId?: string;
  weightClass?: number;
  warrantyPeriod?: string;
  suppliedID?: string;
  productStatus?: number;
  listPrice?: string;
  minPrice?: number;
  catalogUrl?: string;
}

export const defaultValue: Readonly<IProductInfomation> = {};
