import { IOrder } from 'app/shared/model/order.model';
import { IProductInfomation } from 'app/shared/model/product-infomation.model';

export interface IOrderItem {
  id?: number;
  lineItemId?: string;
  unitPrice?: number;
  quantity?: number;
  order?: IOrder;
  product?: IProductInfomation;
}

export const defaultValue: Readonly<IOrderItem> = {};
