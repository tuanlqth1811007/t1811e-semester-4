import { Moment } from 'moment';
import { IEmployee } from 'app/shared/model/employee.model';
import { ICustomer } from 'app/shared/model/customer.model';

export interface IOrder {
  id?: number;
  orderDate?: string;
  orderMode?: string;
  orderStatus?: number;
  orderTotal?: number;
  promotionId?: string;
  salesRepId?: IEmployee;
  customer?: ICustomer;
}

export const defaultValue: Readonly<IOrder> = {};
