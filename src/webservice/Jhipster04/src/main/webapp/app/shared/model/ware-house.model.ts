export interface IWareHouse {
  id?: number;
  wareHouseSpec?: string;
  wareHouseName?: string;
  whGeoLocation?: string;
}

export const defaultValue: Readonly<IWareHouse> = {};
