import { Moment } from 'moment';
import { IEmployee } from 'app/shared/model/employee.model';
import { IJob } from 'app/shared/model/job.model';
import { IDepartment } from 'app/shared/model/department.model';

export interface IJobHistory {
  id?: number;
  startDate?: string;
  endDate?: string;
  employee?: IEmployee;
  job?: IJob;
  department?: IDepartment;
}

export const defaultValue: Readonly<IJobHistory> = {};
