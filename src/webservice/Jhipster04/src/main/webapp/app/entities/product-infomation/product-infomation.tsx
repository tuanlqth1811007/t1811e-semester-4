import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './product-infomation.reducer';
import { IProductInfomation } from 'app/shared/model/product-infomation.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProductInfomationProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const ProductInfomation = (props: IProductInfomationProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { productInfomationList, match, loading } = props;
  return (
    <div>
      <h2 id="product-infomation-heading">
        Product Infomations
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp; Create new Product Infomation
        </Link>
      </h2>
      <div className="table-responsive">
        {productInfomationList && productInfomationList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Product Name</th>
                <th>Product Description</th>
                <th>Category Id</th>
                <th>Weight Class</th>
                <th>Warranty Period</th>
                <th>Supplied ID</th>
                <th>Product Status</th>
                <th>List Price</th>
                <th>Min Price</th>
                <th>Catalog Url</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {productInfomationList.map((productInfomation, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${productInfomation.id}`} color="link" size="sm">
                      {productInfomation.id}
                    </Button>
                  </td>
                  <td>{productInfomation.productName}</td>
                  <td>{productInfomation.productDescription}</td>
                  <td>{productInfomation.categoryId}</td>
                  <td>{productInfomation.weightClass}</td>
                  <td>{productInfomation.warrantyPeriod}</td>
                  <td>{productInfomation.suppliedID}</td>
                  <td>{productInfomation.productStatus}</td>
                  <td>{productInfomation.listPrice}</td>
                  <td>{productInfomation.minPrice}</td>
                  <td>{productInfomation.catalogUrl}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${productInfomation.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${productInfomation.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${productInfomation.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Product Infomations found</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ productInfomation }: IRootState) => ({
  productInfomationList: productInfomation.entities,
  loading: productInfomation.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ProductInfomation);
