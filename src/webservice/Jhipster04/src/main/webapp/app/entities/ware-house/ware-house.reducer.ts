import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IWareHouse, defaultValue } from 'app/shared/model/ware-house.model';

export const ACTION_TYPES = {
  FETCH_WAREHOUSE_LIST: 'wareHouse/FETCH_WAREHOUSE_LIST',
  FETCH_WAREHOUSE: 'wareHouse/FETCH_WAREHOUSE',
  CREATE_WAREHOUSE: 'wareHouse/CREATE_WAREHOUSE',
  UPDATE_WAREHOUSE: 'wareHouse/UPDATE_WAREHOUSE',
  DELETE_WAREHOUSE: 'wareHouse/DELETE_WAREHOUSE',
  RESET: 'wareHouse/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IWareHouse>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type WareHouseState = Readonly<typeof initialState>;

// Reducer

export default (state: WareHouseState = initialState, action): WareHouseState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_WAREHOUSE_LIST):
    case REQUEST(ACTION_TYPES.FETCH_WAREHOUSE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_WAREHOUSE):
    case REQUEST(ACTION_TYPES.UPDATE_WAREHOUSE):
    case REQUEST(ACTION_TYPES.DELETE_WAREHOUSE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_WAREHOUSE_LIST):
    case FAILURE(ACTION_TYPES.FETCH_WAREHOUSE):
    case FAILURE(ACTION_TYPES.CREATE_WAREHOUSE):
    case FAILURE(ACTION_TYPES.UPDATE_WAREHOUSE):
    case FAILURE(ACTION_TYPES.DELETE_WAREHOUSE):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_WAREHOUSE_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_WAREHOUSE):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_WAREHOUSE):
    case SUCCESS(ACTION_TYPES.UPDATE_WAREHOUSE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_WAREHOUSE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/ware-houses';

// Actions

export const getEntities: ICrudGetAllAction<IWareHouse> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_WAREHOUSE_LIST,
  payload: axios.get<IWareHouse>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IWareHouse> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_WAREHOUSE,
    payload: axios.get<IWareHouse>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IWareHouse> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_WAREHOUSE,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IWareHouse> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_WAREHOUSE,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IWareHouse> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_WAREHOUSE,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
