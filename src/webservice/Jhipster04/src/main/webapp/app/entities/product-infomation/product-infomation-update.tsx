import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './product-infomation.reducer';
import { IProductInfomation } from 'app/shared/model/product-infomation.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProductInfomationUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ProductInfomationUpdate = (props: IProductInfomationUpdateProps) => {
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { productInfomationEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/product-infomation');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...productInfomationEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="jhipster04App.productInfomation.home.createOrEditLabel">Create or edit a ProductInfomation</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : productInfomationEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="product-infomation-id">ID</Label>
                  <AvInput id="product-infomation-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="productNameLabel" for="product-infomation-productName">
                  Product Name
                </Label>
                <AvField id="product-infomation-productName" type="text" name="productName" />
              </AvGroup>
              <AvGroup>
                <Label id="productDescriptionLabel" for="product-infomation-productDescription">
                  Product Description
                </Label>
                <AvField id="product-infomation-productDescription" type="text" name="productDescription" />
              </AvGroup>
              <AvGroup>
                <Label id="categoryIdLabel" for="product-infomation-categoryId">
                  Category Id
                </Label>
                <AvField id="product-infomation-categoryId" type="text" name="categoryId" />
              </AvGroup>
              <AvGroup>
                <Label id="weightClassLabel" for="product-infomation-weightClass">
                  Weight Class
                </Label>
                <AvField id="product-infomation-weightClass" type="string" className="form-control" name="weightClass" />
              </AvGroup>
              <AvGroup>
                <Label id="warrantyPeriodLabel" for="product-infomation-warrantyPeriod">
                  Warranty Period
                </Label>
                <AvField id="product-infomation-warrantyPeriod" type="text" name="warrantyPeriod" />
              </AvGroup>
              <AvGroup>
                <Label id="suppliedIDLabel" for="product-infomation-suppliedID">
                  Supplied ID
                </Label>
                <AvField id="product-infomation-suppliedID" type="text" name="suppliedID" />
              </AvGroup>
              <AvGroup>
                <Label id="productStatusLabel" for="product-infomation-productStatus">
                  Product Status
                </Label>
                <AvField id="product-infomation-productStatus" type="string" className="form-control" name="productStatus" />
              </AvGroup>
              <AvGroup>
                <Label id="listPriceLabel" for="product-infomation-listPrice">
                  List Price
                </Label>
                <AvField id="product-infomation-listPrice" type="text" name="listPrice" />
              </AvGroup>
              <AvGroup>
                <Label id="minPriceLabel" for="product-infomation-minPrice">
                  Min Price
                </Label>
                <AvField id="product-infomation-minPrice" type="string" className="form-control" name="minPrice" />
              </AvGroup>
              <AvGroup>
                <Label id="catalogUrlLabel" for="product-infomation-catalogUrl">
                  Catalog Url
                </Label>
                <AvField id="product-infomation-catalogUrl" type="text" name="catalogUrl" />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/product-infomation" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  productInfomationEntity: storeState.productInfomation.entity,
  loading: storeState.productInfomation.loading,
  updating: storeState.productInfomation.updating,
  updateSuccess: storeState.productInfomation.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ProductInfomationUpdate);
