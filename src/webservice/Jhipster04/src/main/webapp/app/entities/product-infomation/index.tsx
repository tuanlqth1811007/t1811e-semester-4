import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProductInfomation from './product-infomation';
import ProductInfomationDetail from './product-infomation-detail';
import ProductInfomationUpdate from './product-infomation-update';
import ProductInfomationDeleteDialog from './product-infomation-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProductInfomationUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProductInfomationUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProductInfomationDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProductInfomation} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ProductInfomationDeleteDialog} />
  </>
);

export default Routes;
