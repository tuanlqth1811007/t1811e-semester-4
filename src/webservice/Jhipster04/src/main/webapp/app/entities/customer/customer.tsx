import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './customer.reducer';
import { ICustomer } from 'app/shared/model/customer.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ICustomerProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Customer = (props: ICustomerProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { customerList, match, loading } = props;
  return (
    <div>
      <h2 id="customer-heading">
        Customers
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp; Create new Customer
        </Link>
      </h2>
      <div className="table-responsive">
        {customerList && customerList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Cust First Name</th>
                <th>Cust Last Name</th>
                <th>Cust Address</th>
                <th>Phone Number</th>
                <th>N Lsanguage</th>
                <th>Nls Territory</th>
                <th>Credit Limit</th>
                <th>Cust Email</th>
                <th>Account Mgr Id</th>
                <th>Cust Geo Location</th>
                <th>Date Of Birth</th>
                <th>Marital Status</th>
                <th>Gender</th>
                <th>Income Level</th>
                <th>Employee</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {customerList.map((customer, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${customer.id}`} color="link" size="sm">
                      {customer.id}
                    </Button>
                  </td>
                  <td>{customer.custFirstName}</td>
                  <td>{customer.custLastName}</td>
                  <td>{customer.custAddress}</td>
                  <td>{customer.phoneNumber}</td>
                  <td>{customer.nLsanguage}</td>
                  <td>{customer.nlsTerritory}</td>
                  <td>{customer.creditLimit}</td>
                  <td>{customer.custEmail}</td>
                  <td>{customer.accountMgrId}</td>
                  <td>{customer.custGeoLocation}</td>
                  <td>
                    {customer.dateOfBirth ? <TextFormat type="date" value={customer.dateOfBirth} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>{customer.maritalStatus}</td>
                  <td>{customer.gender}</td>
                  <td>{customer.incomeLevel}</td>
                  <td>{customer.employee ? <Link to={`employee/${customer.employee.id}`}>{customer.employee.id}</Link> : ''}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${customer.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${customer.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${customer.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Customers found</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ customer }: IRootState) => ({
  customerList: customer.entities,
  loading: customer.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Customer);
