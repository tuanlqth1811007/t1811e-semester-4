import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IWareHouse } from 'app/shared/model/ware-house.model';
import { getEntities as getWareHouses } from 'app/entities/ware-house/ware-house.reducer';
import { IProductInfomation } from 'app/shared/model/product-infomation.model';
import { getEntities as getProductInfomations } from 'app/entities/product-infomation/product-infomation.reducer';
import { getEntity, updateEntity, createEntity, reset } from './inventory.reducer';
import { IInventory } from 'app/shared/model/inventory.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IInventoryUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const InventoryUpdate = (props: IInventoryUpdateProps) => {
  const [wareHouseId, setWareHouseId] = useState('0');
  const [productId, setProductId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { inventoryEntity, wareHouses, productInfomations, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/inventory');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getWareHouses();
    props.getProductInfomations();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...inventoryEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="jhipster04App.inventory.home.createOrEditLabel">Create or edit a Inventory</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : inventoryEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="inventory-id">ID</Label>
                  <AvInput id="inventory-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="quantityOfHandLabel" for="inventory-quantityOfHand">
                  Quantity Of Hand
                </Label>
                <AvField id="inventory-quantityOfHand" type="string" className="form-control" name="quantityOfHand" />
              </AvGroup>
              <AvGroup>
                <Label for="inventory-wareHouse">Ware House</Label>
                <AvInput id="inventory-wareHouse" type="select" className="form-control" name="wareHouse.id">
                  <option value="" key="0" />
                  {wareHouses
                    ? wareHouses.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="inventory-product">Product</Label>
                <AvInput id="inventory-product" type="select" className="form-control" name="product.id">
                  <option value="" key="0" />
                  {productInfomations
                    ? productInfomations.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/inventory" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  wareHouses: storeState.wareHouse.entities,
  productInfomations: storeState.productInfomation.entities,
  inventoryEntity: storeState.inventory.entity,
  loading: storeState.inventory.loading,
  updating: storeState.inventory.updating,
  updateSuccess: storeState.inventory.updateSuccess,
});

const mapDispatchToProps = {
  getWareHouses,
  getProductInfomations,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(InventoryUpdate);
