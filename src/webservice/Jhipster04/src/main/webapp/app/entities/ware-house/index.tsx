import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import WareHouse from './ware-house';
import WareHouseDetail from './ware-house-detail';
import WareHouseUpdate from './ware-house-update';
import WareHouseDeleteDialog from './ware-house-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={WareHouseUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={WareHouseUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={WareHouseDetail} />
      <ErrorBoundaryRoute path={match.url} component={WareHouse} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={WareHouseDeleteDialog} />
  </>
);

export default Routes;
