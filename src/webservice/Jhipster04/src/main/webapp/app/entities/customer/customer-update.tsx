import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IEmployee } from 'app/shared/model/employee.model';
import { getEntities as getEmployees } from 'app/entities/employee/employee.reducer';
import { getEntity, updateEntity, createEntity, reset } from './customer.reducer';
import { ICustomer } from 'app/shared/model/customer.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ICustomerUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const CustomerUpdate = (props: ICustomerUpdateProps) => {
  const [employeeId, setEmployeeId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { customerEntity, employees, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/customer');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getEmployees();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...customerEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="jhipster04App.customer.home.createOrEditLabel">Create or edit a Customer</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : customerEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="customer-id">ID</Label>
                  <AvInput id="customer-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="custFirstNameLabel" for="customer-custFirstName">
                  Cust First Name
                </Label>
                <AvField id="customer-custFirstName" type="text" name="custFirstName" />
              </AvGroup>
              <AvGroup>
                <Label id="custLastNameLabel" for="customer-custLastName">
                  Cust Last Name
                </Label>
                <AvField id="customer-custLastName" type="text" name="custLastName" />
              </AvGroup>
              <AvGroup>
                <Label id="custAddressLabel" for="customer-custAddress">
                  Cust Address
                </Label>
                <AvField id="customer-custAddress" type="text" name="custAddress" />
              </AvGroup>
              <AvGroup>
                <Label id="phoneNumberLabel" for="customer-phoneNumber">
                  Phone Number
                </Label>
                <AvField id="customer-phoneNumber" type="text" name="phoneNumber" />
              </AvGroup>
              <AvGroup>
                <Label id="nLsanguageLabel" for="customer-nLsanguage">
                  N Lsanguage
                </Label>
                <AvField id="customer-nLsanguage" type="text" name="nLsanguage" />
              </AvGroup>
              <AvGroup>
                <Label id="nlsTerritoryLabel" for="customer-nlsTerritory">
                  Nls Territory
                </Label>
                <AvField id="customer-nlsTerritory" type="text" name="nlsTerritory" />
              </AvGroup>
              <AvGroup>
                <Label id="creditLimitLabel" for="customer-creditLimit">
                  Credit Limit
                </Label>
                <AvField id="customer-creditLimit" type="string" className="form-control" name="creditLimit" />
              </AvGroup>
              <AvGroup>
                <Label id="custEmailLabel" for="customer-custEmail">
                  Cust Email
                </Label>
                <AvField id="customer-custEmail" type="text" name="custEmail" />
              </AvGroup>
              <AvGroup>
                <Label id="accountMgrIdLabel" for="customer-accountMgrId">
                  Account Mgr Id
                </Label>
                <AvField id="customer-accountMgrId" type="text" name="accountMgrId" />
              </AvGroup>
              <AvGroup>
                <Label id="custGeoLocationLabel" for="customer-custGeoLocation">
                  Cust Geo Location
                </Label>
                <AvField id="customer-custGeoLocation" type="text" name="custGeoLocation" />
              </AvGroup>
              <AvGroup>
                <Label id="dateOfBirthLabel" for="customer-dateOfBirth">
                  Date Of Birth
                </Label>
                <AvField id="customer-dateOfBirth" type="date" className="form-control" name="dateOfBirth" />
              </AvGroup>
              <AvGroup>
                <Label id="maritalStatusLabel" for="customer-maritalStatus">
                  Marital Status
                </Label>
                <AvField id="customer-maritalStatus" type="string" className="form-control" name="maritalStatus" />
              </AvGroup>
              <AvGroup>
                <Label id="genderLabel" for="customer-gender">
                  Gender
                </Label>
                <AvField id="customer-gender" type="string" className="form-control" name="gender" />
              </AvGroup>
              <AvGroup>
                <Label id="incomeLevelLabel" for="customer-incomeLevel">
                  Income Level
                </Label>
                <AvField id="customer-incomeLevel" type="string" className="form-control" name="incomeLevel" />
              </AvGroup>
              <AvGroup>
                <Label for="customer-employee">Employee</Label>
                <AvInput id="customer-employee" type="select" className="form-control" name="employee.id">
                  <option value="" key="0" />
                  {employees
                    ? employees.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/customer" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  employees: storeState.employee.entities,
  customerEntity: storeState.customer.entity,
  loading: storeState.customer.loading,
  updating: storeState.customer.updating,
  updateSuccess: storeState.customer.updateSuccess,
});

const mapDispatchToProps = {
  getEmployees,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(CustomerUpdate);
