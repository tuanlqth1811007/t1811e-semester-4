import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './ware-house.reducer';
import { IWareHouse } from 'app/shared/model/ware-house.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IWareHouseProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const WareHouse = (props: IWareHouseProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { wareHouseList, match, loading } = props;
  return (
    <div>
      <h2 id="ware-house-heading">
        Ware Houses
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp; Create new Ware House
        </Link>
      </h2>
      <div className="table-responsive">
        {wareHouseList && wareHouseList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Ware House Spec</th>
                <th>Ware House Name</th>
                <th>Wh Geo Location</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {wareHouseList.map((wareHouse, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${wareHouse.id}`} color="link" size="sm">
                      {wareHouse.id}
                    </Button>
                  </td>
                  <td>{wareHouse.wareHouseSpec}</td>
                  <td>{wareHouse.wareHouseName}</td>
                  <td>{wareHouse.whGeoLocation}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${wareHouse.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${wareHouse.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${wareHouse.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Ware Houses found</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ wareHouse }: IRootState) => ({
  wareHouseList: wareHouse.entities,
  loading: wareHouse.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(WareHouse);
