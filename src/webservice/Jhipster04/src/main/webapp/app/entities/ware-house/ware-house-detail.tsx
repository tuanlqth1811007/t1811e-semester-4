import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './ware-house.reducer';
import { IWareHouse } from 'app/shared/model/ware-house.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IWareHouseDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const WareHouseDetail = (props: IWareHouseDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { wareHouseEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          WareHouse [<b>{wareHouseEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="wareHouseSpec">Ware House Spec</span>
          </dt>
          <dd>{wareHouseEntity.wareHouseSpec}</dd>
          <dt>
            <span id="wareHouseName">Ware House Name</span>
          </dt>
          <dd>{wareHouseEntity.wareHouseName}</dd>
          <dt>
            <span id="whGeoLocation">Wh Geo Location</span>
          </dt>
          <dd>{wareHouseEntity.whGeoLocation}</dd>
        </dl>
        <Button tag={Link} to="/ware-house" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/ware-house/${wareHouseEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ wareHouse }: IRootState) => ({
  wareHouseEntity: wareHouse.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(WareHouseDetail);
