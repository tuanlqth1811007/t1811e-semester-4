import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProductInfomation, defaultValue } from 'app/shared/model/product-infomation.model';

export const ACTION_TYPES = {
  FETCH_PRODUCTINFOMATION_LIST: 'productInfomation/FETCH_PRODUCTINFOMATION_LIST',
  FETCH_PRODUCTINFOMATION: 'productInfomation/FETCH_PRODUCTINFOMATION',
  CREATE_PRODUCTINFOMATION: 'productInfomation/CREATE_PRODUCTINFOMATION',
  UPDATE_PRODUCTINFOMATION: 'productInfomation/UPDATE_PRODUCTINFOMATION',
  DELETE_PRODUCTINFOMATION: 'productInfomation/DELETE_PRODUCTINFOMATION',
  RESET: 'productInfomation/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProductInfomation>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type ProductInfomationState = Readonly<typeof initialState>;

// Reducer

export default (state: ProductInfomationState = initialState, action): ProductInfomationState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PRODUCTINFOMATION_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PRODUCTINFOMATION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_PRODUCTINFOMATION):
    case REQUEST(ACTION_TYPES.UPDATE_PRODUCTINFOMATION):
    case REQUEST(ACTION_TYPES.DELETE_PRODUCTINFOMATION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_PRODUCTINFOMATION_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PRODUCTINFOMATION):
    case FAILURE(ACTION_TYPES.CREATE_PRODUCTINFOMATION):
    case FAILURE(ACTION_TYPES.UPDATE_PRODUCTINFOMATION):
    case FAILURE(ACTION_TYPES.DELETE_PRODUCTINFOMATION):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_PRODUCTINFOMATION_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_PRODUCTINFOMATION):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_PRODUCTINFOMATION):
    case SUCCESS(ACTION_TYPES.UPDATE_PRODUCTINFOMATION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_PRODUCTINFOMATION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/product-infomations';

// Actions

export const getEntities: ICrudGetAllAction<IProductInfomation> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_PRODUCTINFOMATION_LIST,
  payload: axios.get<IProductInfomation>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IProductInfomation> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PRODUCTINFOMATION,
    payload: axios.get<IProductInfomation>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IProductInfomation> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PRODUCTINFOMATION,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IProductInfomation> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PRODUCTINFOMATION,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProductInfomation> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PRODUCTINFOMATION,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
