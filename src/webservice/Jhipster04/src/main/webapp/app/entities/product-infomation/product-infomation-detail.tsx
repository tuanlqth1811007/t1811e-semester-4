import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './product-infomation.reducer';
import { IProductInfomation } from 'app/shared/model/product-infomation.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProductInfomationDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ProductInfomationDetail = (props: IProductInfomationDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { productInfomationEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          ProductInfomation [<b>{productInfomationEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="productName">Product Name</span>
          </dt>
          <dd>{productInfomationEntity.productName}</dd>
          <dt>
            <span id="productDescription">Product Description</span>
          </dt>
          <dd>{productInfomationEntity.productDescription}</dd>
          <dt>
            <span id="categoryId">Category Id</span>
          </dt>
          <dd>{productInfomationEntity.categoryId}</dd>
          <dt>
            <span id="weightClass">Weight Class</span>
          </dt>
          <dd>{productInfomationEntity.weightClass}</dd>
          <dt>
            <span id="warrantyPeriod">Warranty Period</span>
          </dt>
          <dd>{productInfomationEntity.warrantyPeriod}</dd>
          <dt>
            <span id="suppliedID">Supplied ID</span>
          </dt>
          <dd>{productInfomationEntity.suppliedID}</dd>
          <dt>
            <span id="productStatus">Product Status</span>
          </dt>
          <dd>{productInfomationEntity.productStatus}</dd>
          <dt>
            <span id="listPrice">List Price</span>
          </dt>
          <dd>{productInfomationEntity.listPrice}</dd>
          <dt>
            <span id="minPrice">Min Price</span>
          </dt>
          <dd>{productInfomationEntity.minPrice}</dd>
          <dt>
            <span id="catalogUrl">Catalog Url</span>
          </dt>
          <dd>{productInfomationEntity.catalogUrl}</dd>
        </dl>
        <Button tag={Link} to="/product-infomation" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/product-infomation/${productInfomationEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ productInfomation }: IRootState) => ({
  productInfomationEntity: productInfomation.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ProductInfomationDetail);
