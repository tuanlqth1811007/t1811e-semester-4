import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './customer.reducer';
import { ICustomer } from 'app/shared/model/customer.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ICustomerDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const CustomerDetail = (props: ICustomerDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { customerEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          Customer [<b>{customerEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="custFirstName">Cust First Name</span>
          </dt>
          <dd>{customerEntity.custFirstName}</dd>
          <dt>
            <span id="custLastName">Cust Last Name</span>
          </dt>
          <dd>{customerEntity.custLastName}</dd>
          <dt>
            <span id="custAddress">Cust Address</span>
          </dt>
          <dd>{customerEntity.custAddress}</dd>
          <dt>
            <span id="phoneNumber">Phone Number</span>
          </dt>
          <dd>{customerEntity.phoneNumber}</dd>
          <dt>
            <span id="nLsanguage">N Lsanguage</span>
          </dt>
          <dd>{customerEntity.nLsanguage}</dd>
          <dt>
            <span id="nlsTerritory">Nls Territory</span>
          </dt>
          <dd>{customerEntity.nlsTerritory}</dd>
          <dt>
            <span id="creditLimit">Credit Limit</span>
          </dt>
          <dd>{customerEntity.creditLimit}</dd>
          <dt>
            <span id="custEmail">Cust Email</span>
          </dt>
          <dd>{customerEntity.custEmail}</dd>
          <dt>
            <span id="accountMgrId">Account Mgr Id</span>
          </dt>
          <dd>{customerEntity.accountMgrId}</dd>
          <dt>
            <span id="custGeoLocation">Cust Geo Location</span>
          </dt>
          <dd>{customerEntity.custGeoLocation}</dd>
          <dt>
            <span id="dateOfBirth">Date Of Birth</span>
          </dt>
          <dd>
            {customerEntity.dateOfBirth ? (
              <TextFormat value={customerEntity.dateOfBirth} type="date" format={APP_LOCAL_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="maritalStatus">Marital Status</span>
          </dt>
          <dd>{customerEntity.maritalStatus}</dd>
          <dt>
            <span id="gender">Gender</span>
          </dt>
          <dd>{customerEntity.gender}</dd>
          <dt>
            <span id="incomeLevel">Income Level</span>
          </dt>
          <dd>{customerEntity.incomeLevel}</dd>
          <dt>Employee</dt>
          <dd>{customerEntity.employee ? customerEntity.employee.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/customer" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/customer/${customerEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ customer }: IRootState) => ({
  customerEntity: customer.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(CustomerDetail);
