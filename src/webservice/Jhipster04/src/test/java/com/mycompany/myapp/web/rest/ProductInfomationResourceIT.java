package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.Jhipster04App;
import com.mycompany.myapp.domain.ProductInfomation;
import com.mycompany.myapp.repository.ProductInfomationRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ProductInfomationResource} REST controller.
 */
@SpringBootTest(classes = Jhipster04App.class)
@AutoConfigureMockMvc
@WithMockUser
public class ProductInfomationResourceIT {

    private static final String DEFAULT_PRODUCT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_PRODUCT_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PRODUCT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_PRODUCT_DESCRIPTION = "BBBBBBBBBB";

    private static final UUID DEFAULT_CATEGORY_ID = UUID.randomUUID();
    private static final UUID UPDATED_CATEGORY_ID = UUID.randomUUID();

    private static final Integer DEFAULT_WEIGHT_CLASS = 1;
    private static final Integer UPDATED_WEIGHT_CLASS = 2;

    private static final String DEFAULT_WARRANTY_PERIOD = "AAAAAAAAAA";
    private static final String UPDATED_WARRANTY_PERIOD = "BBBBBBBBBB";

    private static final UUID DEFAULT_SUPPLIED_ID = UUID.randomUUID();
    private static final UUID UPDATED_SUPPLIED_ID = UUID.randomUUID();

    private static final Integer DEFAULT_PRODUCT_STATUS = 1;
    private static final Integer UPDATED_PRODUCT_STATUS = 2;

    private static final String DEFAULT_LIST_PRICE = "AAAAAAAAAA";
    private static final String UPDATED_LIST_PRICE = "BBBBBBBBBB";

    private static final Float DEFAULT_MIN_PRICE = 1F;
    private static final Float UPDATED_MIN_PRICE = 2F;

    private static final String DEFAULT_CATALOG_URL = "AAAAAAAAAA";
    private static final String UPDATED_CATALOG_URL = "BBBBBBBBBB";

    @Autowired
    private ProductInfomationRepository productInfomationRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restProductInfomationMockMvc;

    private ProductInfomation productInfomation;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductInfomation createEntity(EntityManager em) {
        ProductInfomation productInfomation = new ProductInfomation()
            .productName(DEFAULT_PRODUCT_NAME)
            .productDescription(DEFAULT_PRODUCT_DESCRIPTION)
            .categoryId(DEFAULT_CATEGORY_ID)
            .weightClass(DEFAULT_WEIGHT_CLASS)
            .warrantyPeriod(DEFAULT_WARRANTY_PERIOD)
            .suppliedID(DEFAULT_SUPPLIED_ID)
            .productStatus(DEFAULT_PRODUCT_STATUS)
            .listPrice(DEFAULT_LIST_PRICE)
            .minPrice(DEFAULT_MIN_PRICE)
            .catalogUrl(DEFAULT_CATALOG_URL);
        return productInfomation;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductInfomation createUpdatedEntity(EntityManager em) {
        ProductInfomation productInfomation = new ProductInfomation()
            .productName(UPDATED_PRODUCT_NAME)
            .productDescription(UPDATED_PRODUCT_DESCRIPTION)
            .categoryId(UPDATED_CATEGORY_ID)
            .weightClass(UPDATED_WEIGHT_CLASS)
            .warrantyPeriod(UPDATED_WARRANTY_PERIOD)
            .suppliedID(UPDATED_SUPPLIED_ID)
            .productStatus(UPDATED_PRODUCT_STATUS)
            .listPrice(UPDATED_LIST_PRICE)
            .minPrice(UPDATED_MIN_PRICE)
            .catalogUrl(UPDATED_CATALOG_URL);
        return productInfomation;
    }

    @BeforeEach
    public void initTest() {
        productInfomation = createEntity(em);
    }

    @Test
    @Transactional
    public void createProductInfomation() throws Exception {
        int databaseSizeBeforeCreate = productInfomationRepository.findAll().size();
        // Create the ProductInfomation
        restProductInfomationMockMvc.perform(post("/api/product-infomations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productInfomation)))
            .andExpect(status().isCreated());

        // Validate the ProductInfomation in the database
        List<ProductInfomation> productInfomationList = productInfomationRepository.findAll();
        assertThat(productInfomationList).hasSize(databaseSizeBeforeCreate + 1);
        ProductInfomation testProductInfomation = productInfomationList.get(productInfomationList.size() - 1);
        assertThat(testProductInfomation.getProductName()).isEqualTo(DEFAULT_PRODUCT_NAME);
        assertThat(testProductInfomation.getProductDescription()).isEqualTo(DEFAULT_PRODUCT_DESCRIPTION);
        assertThat(testProductInfomation.getCategoryId()).isEqualTo(DEFAULT_CATEGORY_ID);
        assertThat(testProductInfomation.getWeightClass()).isEqualTo(DEFAULT_WEIGHT_CLASS);
        assertThat(testProductInfomation.getWarrantyPeriod()).isEqualTo(DEFAULT_WARRANTY_PERIOD);
        assertThat(testProductInfomation.getSuppliedID()).isEqualTo(DEFAULT_SUPPLIED_ID);
        assertThat(testProductInfomation.getProductStatus()).isEqualTo(DEFAULT_PRODUCT_STATUS);
        assertThat(testProductInfomation.getListPrice()).isEqualTo(DEFAULT_LIST_PRICE);
        assertThat(testProductInfomation.getMinPrice()).isEqualTo(DEFAULT_MIN_PRICE);
        assertThat(testProductInfomation.getCatalogUrl()).isEqualTo(DEFAULT_CATALOG_URL);
    }

    @Test
    @Transactional
    public void createProductInfomationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = productInfomationRepository.findAll().size();

        // Create the ProductInfomation with an existing ID
        productInfomation.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProductInfomationMockMvc.perform(post("/api/product-infomations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productInfomation)))
            .andExpect(status().isBadRequest());

        // Validate the ProductInfomation in the database
        List<ProductInfomation> productInfomationList = productInfomationRepository.findAll();
        assertThat(productInfomationList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllProductInfomations() throws Exception {
        // Initialize the database
        productInfomationRepository.saveAndFlush(productInfomation);

        // Get all the productInfomationList
        restProductInfomationMockMvc.perform(get("/api/product-infomations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(productInfomation.getId().intValue())))
            .andExpect(jsonPath("$.[*].productName").value(hasItem(DEFAULT_PRODUCT_NAME)))
            .andExpect(jsonPath("$.[*].productDescription").value(hasItem(DEFAULT_PRODUCT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].categoryId").value(hasItem(DEFAULT_CATEGORY_ID.toString())))
            .andExpect(jsonPath("$.[*].weightClass").value(hasItem(DEFAULT_WEIGHT_CLASS)))
            .andExpect(jsonPath("$.[*].warrantyPeriod").value(hasItem(DEFAULT_WARRANTY_PERIOD)))
            .andExpect(jsonPath("$.[*].suppliedID").value(hasItem(DEFAULT_SUPPLIED_ID.toString())))
            .andExpect(jsonPath("$.[*].productStatus").value(hasItem(DEFAULT_PRODUCT_STATUS)))
            .andExpect(jsonPath("$.[*].listPrice").value(hasItem(DEFAULT_LIST_PRICE)))
            .andExpect(jsonPath("$.[*].minPrice").value(hasItem(DEFAULT_MIN_PRICE.doubleValue())))
            .andExpect(jsonPath("$.[*].catalogUrl").value(hasItem(DEFAULT_CATALOG_URL)));
    }
    
    @Test
    @Transactional
    public void getProductInfomation() throws Exception {
        // Initialize the database
        productInfomationRepository.saveAndFlush(productInfomation);

        // Get the productInfomation
        restProductInfomationMockMvc.perform(get("/api/product-infomations/{id}", productInfomation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(productInfomation.getId().intValue()))
            .andExpect(jsonPath("$.productName").value(DEFAULT_PRODUCT_NAME))
            .andExpect(jsonPath("$.productDescription").value(DEFAULT_PRODUCT_DESCRIPTION))
            .andExpect(jsonPath("$.categoryId").value(DEFAULT_CATEGORY_ID.toString()))
            .andExpect(jsonPath("$.weightClass").value(DEFAULT_WEIGHT_CLASS))
            .andExpect(jsonPath("$.warrantyPeriod").value(DEFAULT_WARRANTY_PERIOD))
            .andExpect(jsonPath("$.suppliedID").value(DEFAULT_SUPPLIED_ID.toString()))
            .andExpect(jsonPath("$.productStatus").value(DEFAULT_PRODUCT_STATUS))
            .andExpect(jsonPath("$.listPrice").value(DEFAULT_LIST_PRICE))
            .andExpect(jsonPath("$.minPrice").value(DEFAULT_MIN_PRICE.doubleValue()))
            .andExpect(jsonPath("$.catalogUrl").value(DEFAULT_CATALOG_URL));
    }
    @Test
    @Transactional
    public void getNonExistingProductInfomation() throws Exception {
        // Get the productInfomation
        restProductInfomationMockMvc.perform(get("/api/product-infomations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProductInfomation() throws Exception {
        // Initialize the database
        productInfomationRepository.saveAndFlush(productInfomation);

        int databaseSizeBeforeUpdate = productInfomationRepository.findAll().size();

        // Update the productInfomation
        ProductInfomation updatedProductInfomation = productInfomationRepository.findById(productInfomation.getId()).get();
        // Disconnect from session so that the updates on updatedProductInfomation are not directly saved in db
        em.detach(updatedProductInfomation);
        updatedProductInfomation
            .productName(UPDATED_PRODUCT_NAME)
            .productDescription(UPDATED_PRODUCT_DESCRIPTION)
            .categoryId(UPDATED_CATEGORY_ID)
            .weightClass(UPDATED_WEIGHT_CLASS)
            .warrantyPeriod(UPDATED_WARRANTY_PERIOD)
            .suppliedID(UPDATED_SUPPLIED_ID)
            .productStatus(UPDATED_PRODUCT_STATUS)
            .listPrice(UPDATED_LIST_PRICE)
            .minPrice(UPDATED_MIN_PRICE)
            .catalogUrl(UPDATED_CATALOG_URL);

        restProductInfomationMockMvc.perform(put("/api/product-infomations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedProductInfomation)))
            .andExpect(status().isOk());

        // Validate the ProductInfomation in the database
        List<ProductInfomation> productInfomationList = productInfomationRepository.findAll();
        assertThat(productInfomationList).hasSize(databaseSizeBeforeUpdate);
        ProductInfomation testProductInfomation = productInfomationList.get(productInfomationList.size() - 1);
        assertThat(testProductInfomation.getProductName()).isEqualTo(UPDATED_PRODUCT_NAME);
        assertThat(testProductInfomation.getProductDescription()).isEqualTo(UPDATED_PRODUCT_DESCRIPTION);
        assertThat(testProductInfomation.getCategoryId()).isEqualTo(UPDATED_CATEGORY_ID);
        assertThat(testProductInfomation.getWeightClass()).isEqualTo(UPDATED_WEIGHT_CLASS);
        assertThat(testProductInfomation.getWarrantyPeriod()).isEqualTo(UPDATED_WARRANTY_PERIOD);
        assertThat(testProductInfomation.getSuppliedID()).isEqualTo(UPDATED_SUPPLIED_ID);
        assertThat(testProductInfomation.getProductStatus()).isEqualTo(UPDATED_PRODUCT_STATUS);
        assertThat(testProductInfomation.getListPrice()).isEqualTo(UPDATED_LIST_PRICE);
        assertThat(testProductInfomation.getMinPrice()).isEqualTo(UPDATED_MIN_PRICE);
        assertThat(testProductInfomation.getCatalogUrl()).isEqualTo(UPDATED_CATALOG_URL);
    }

    @Test
    @Transactional
    public void updateNonExistingProductInfomation() throws Exception {
        int databaseSizeBeforeUpdate = productInfomationRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProductInfomationMockMvc.perform(put("/api/product-infomations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productInfomation)))
            .andExpect(status().isBadRequest());

        // Validate the ProductInfomation in the database
        List<ProductInfomation> productInfomationList = productInfomationRepository.findAll();
        assertThat(productInfomationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProductInfomation() throws Exception {
        // Initialize the database
        productInfomationRepository.saveAndFlush(productInfomation);

        int databaseSizeBeforeDelete = productInfomationRepository.findAll().size();

        // Delete the productInfomation
        restProductInfomationMockMvc.perform(delete("/api/product-infomations/{id}", productInfomation.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ProductInfomation> productInfomationList = productInfomationRepository.findAll();
        assertThat(productInfomationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
