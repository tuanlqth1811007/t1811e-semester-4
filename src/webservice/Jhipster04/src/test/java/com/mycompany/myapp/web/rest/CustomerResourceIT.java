package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.Jhipster04App;
import com.mycompany.myapp.domain.Customer;
import com.mycompany.myapp.repository.CustomerRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CustomerResource} REST controller.
 */
@SpringBootTest(classes = Jhipster04App.class)
@AutoConfigureMockMvc
@WithMockUser
public class CustomerResourceIT {

    private static final String DEFAULT_CUST_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CUST_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CUST_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CUST_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CUST_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_CUST_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_PHONE_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_N_LSANGUAGE = "AAAAAAAAAA";
    private static final String UPDATED_N_LSANGUAGE = "BBBBBBBBBB";

    private static final String DEFAULT_NLS_TERRITORY = "AAAAAAAAAA";
    private static final String UPDATED_NLS_TERRITORY = "BBBBBBBBBB";

    private static final Float DEFAULT_CREDIT_LIMIT = 1F;
    private static final Float UPDATED_CREDIT_LIMIT = 2F;

    private static final String DEFAULT_CUST_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_CUST_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_ACCOUNT_MGR_ID = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_MGR_ID = "BBBBBBBBBB";

    private static final String DEFAULT_CUST_GEO_LOCATION = "AAAAAAAAAA";
    private static final String UPDATED_CUST_GEO_LOCATION = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_OF_BIRTH = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_OF_BIRTH = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_MARITAL_STATUS = 1;
    private static final Integer UPDATED_MARITAL_STATUS = 2;

    private static final Integer DEFAULT_GENDER = 1;
    private static final Integer UPDATED_GENDER = 2;

    private static final Integer DEFAULT_INCOME_LEVEL = 1;
    private static final Integer UPDATED_INCOME_LEVEL = 2;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCustomerMockMvc;

    private Customer customer;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Customer createEntity(EntityManager em) {
        Customer customer = new Customer()
            .custFirstName(DEFAULT_CUST_FIRST_NAME)
            .custLastName(DEFAULT_CUST_LAST_NAME)
            .custAddress(DEFAULT_CUST_ADDRESS)
            .phoneNumber(DEFAULT_PHONE_NUMBER)
            .nLsanguage(DEFAULT_N_LSANGUAGE)
            .nlsTerritory(DEFAULT_NLS_TERRITORY)
            .creditLimit(DEFAULT_CREDIT_LIMIT)
            .custEmail(DEFAULT_CUST_EMAIL)
            .accountMgrId(DEFAULT_ACCOUNT_MGR_ID)
            .custGeoLocation(DEFAULT_CUST_GEO_LOCATION)
            .dateOfBirth(DEFAULT_DATE_OF_BIRTH)
            .maritalStatus(DEFAULT_MARITAL_STATUS)
            .gender(DEFAULT_GENDER)
            .incomeLevel(DEFAULT_INCOME_LEVEL);
        return customer;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Customer createUpdatedEntity(EntityManager em) {
        Customer customer = new Customer()
            .custFirstName(UPDATED_CUST_FIRST_NAME)
            .custLastName(UPDATED_CUST_LAST_NAME)
            .custAddress(UPDATED_CUST_ADDRESS)
            .phoneNumber(UPDATED_PHONE_NUMBER)
            .nLsanguage(UPDATED_N_LSANGUAGE)
            .nlsTerritory(UPDATED_NLS_TERRITORY)
            .creditLimit(UPDATED_CREDIT_LIMIT)
            .custEmail(UPDATED_CUST_EMAIL)
            .accountMgrId(UPDATED_ACCOUNT_MGR_ID)
            .custGeoLocation(UPDATED_CUST_GEO_LOCATION)
            .dateOfBirth(UPDATED_DATE_OF_BIRTH)
            .maritalStatus(UPDATED_MARITAL_STATUS)
            .gender(UPDATED_GENDER)
            .incomeLevel(UPDATED_INCOME_LEVEL);
        return customer;
    }

    @BeforeEach
    public void initTest() {
        customer = createEntity(em);
    }

    @Test
    @Transactional
    public void createCustomer() throws Exception {
        int databaseSizeBeforeCreate = customerRepository.findAll().size();
        // Create the Customer
        restCustomerMockMvc.perform(post("/api/customers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(customer)))
            .andExpect(status().isCreated());

        // Validate the Customer in the database
        List<Customer> customerList = customerRepository.findAll();
        assertThat(customerList).hasSize(databaseSizeBeforeCreate + 1);
        Customer testCustomer = customerList.get(customerList.size() - 1);
        assertThat(testCustomer.getCustFirstName()).isEqualTo(DEFAULT_CUST_FIRST_NAME);
        assertThat(testCustomer.getCustLastName()).isEqualTo(DEFAULT_CUST_LAST_NAME);
        assertThat(testCustomer.getCustAddress()).isEqualTo(DEFAULT_CUST_ADDRESS);
        assertThat(testCustomer.getPhoneNumber()).isEqualTo(DEFAULT_PHONE_NUMBER);
        assertThat(testCustomer.getnLsanguage()).isEqualTo(DEFAULT_N_LSANGUAGE);
        assertThat(testCustomer.getNlsTerritory()).isEqualTo(DEFAULT_NLS_TERRITORY);
        assertThat(testCustomer.getCreditLimit()).isEqualTo(DEFAULT_CREDIT_LIMIT);
        assertThat(testCustomer.getCustEmail()).isEqualTo(DEFAULT_CUST_EMAIL);
        assertThat(testCustomer.getAccountMgrId()).isEqualTo(DEFAULT_ACCOUNT_MGR_ID);
        assertThat(testCustomer.getCustGeoLocation()).isEqualTo(DEFAULT_CUST_GEO_LOCATION);
        assertThat(testCustomer.getDateOfBirth()).isEqualTo(DEFAULT_DATE_OF_BIRTH);
        assertThat(testCustomer.getMaritalStatus()).isEqualTo(DEFAULT_MARITAL_STATUS);
        assertThat(testCustomer.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testCustomer.getIncomeLevel()).isEqualTo(DEFAULT_INCOME_LEVEL);
    }

    @Test
    @Transactional
    public void createCustomerWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = customerRepository.findAll().size();

        // Create the Customer with an existing ID
        customer.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCustomerMockMvc.perform(post("/api/customers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(customer)))
            .andExpect(status().isBadRequest());

        // Validate the Customer in the database
        List<Customer> customerList = customerRepository.findAll();
        assertThat(customerList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCustomers() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList
        restCustomerMockMvc.perform(get("/api/customers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(customer.getId().intValue())))
            .andExpect(jsonPath("$.[*].custFirstName").value(hasItem(DEFAULT_CUST_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].custLastName").value(hasItem(DEFAULT_CUST_LAST_NAME)))
            .andExpect(jsonPath("$.[*].custAddress").value(hasItem(DEFAULT_CUST_ADDRESS)))
            .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER)))
            .andExpect(jsonPath("$.[*].nLsanguage").value(hasItem(DEFAULT_N_LSANGUAGE)))
            .andExpect(jsonPath("$.[*].nlsTerritory").value(hasItem(DEFAULT_NLS_TERRITORY)))
            .andExpect(jsonPath("$.[*].creditLimit").value(hasItem(DEFAULT_CREDIT_LIMIT.doubleValue())))
            .andExpect(jsonPath("$.[*].custEmail").value(hasItem(DEFAULT_CUST_EMAIL)))
            .andExpect(jsonPath("$.[*].accountMgrId").value(hasItem(DEFAULT_ACCOUNT_MGR_ID)))
            .andExpect(jsonPath("$.[*].custGeoLocation").value(hasItem(DEFAULT_CUST_GEO_LOCATION)))
            .andExpect(jsonPath("$.[*].dateOfBirth").value(hasItem(DEFAULT_DATE_OF_BIRTH.toString())))
            .andExpect(jsonPath("$.[*].maritalStatus").value(hasItem(DEFAULT_MARITAL_STATUS)))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER)))
            .andExpect(jsonPath("$.[*].incomeLevel").value(hasItem(DEFAULT_INCOME_LEVEL)));
    }
    
    @Test
    @Transactional
    public void getCustomer() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get the customer
        restCustomerMockMvc.perform(get("/api/customers/{id}", customer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(customer.getId().intValue()))
            .andExpect(jsonPath("$.custFirstName").value(DEFAULT_CUST_FIRST_NAME))
            .andExpect(jsonPath("$.custLastName").value(DEFAULT_CUST_LAST_NAME))
            .andExpect(jsonPath("$.custAddress").value(DEFAULT_CUST_ADDRESS))
            .andExpect(jsonPath("$.phoneNumber").value(DEFAULT_PHONE_NUMBER))
            .andExpect(jsonPath("$.nLsanguage").value(DEFAULT_N_LSANGUAGE))
            .andExpect(jsonPath("$.nlsTerritory").value(DEFAULT_NLS_TERRITORY))
            .andExpect(jsonPath("$.creditLimit").value(DEFAULT_CREDIT_LIMIT.doubleValue()))
            .andExpect(jsonPath("$.custEmail").value(DEFAULT_CUST_EMAIL))
            .andExpect(jsonPath("$.accountMgrId").value(DEFAULT_ACCOUNT_MGR_ID))
            .andExpect(jsonPath("$.custGeoLocation").value(DEFAULT_CUST_GEO_LOCATION))
            .andExpect(jsonPath("$.dateOfBirth").value(DEFAULT_DATE_OF_BIRTH.toString()))
            .andExpect(jsonPath("$.maritalStatus").value(DEFAULT_MARITAL_STATUS))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER))
            .andExpect(jsonPath("$.incomeLevel").value(DEFAULT_INCOME_LEVEL));
    }
    @Test
    @Transactional
    public void getNonExistingCustomer() throws Exception {
        // Get the customer
        restCustomerMockMvc.perform(get("/api/customers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCustomer() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        int databaseSizeBeforeUpdate = customerRepository.findAll().size();

        // Update the customer
        Customer updatedCustomer = customerRepository.findById(customer.getId()).get();
        // Disconnect from session so that the updates on updatedCustomer are not directly saved in db
        em.detach(updatedCustomer);
        updatedCustomer
            .custFirstName(UPDATED_CUST_FIRST_NAME)
            .custLastName(UPDATED_CUST_LAST_NAME)
            .custAddress(UPDATED_CUST_ADDRESS)
            .phoneNumber(UPDATED_PHONE_NUMBER)
            .nLsanguage(UPDATED_N_LSANGUAGE)
            .nlsTerritory(UPDATED_NLS_TERRITORY)
            .creditLimit(UPDATED_CREDIT_LIMIT)
            .custEmail(UPDATED_CUST_EMAIL)
            .accountMgrId(UPDATED_ACCOUNT_MGR_ID)
            .custGeoLocation(UPDATED_CUST_GEO_LOCATION)
            .dateOfBirth(UPDATED_DATE_OF_BIRTH)
            .maritalStatus(UPDATED_MARITAL_STATUS)
            .gender(UPDATED_GENDER)
            .incomeLevel(UPDATED_INCOME_LEVEL);

        restCustomerMockMvc.perform(put("/api/customers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCustomer)))
            .andExpect(status().isOk());

        // Validate the Customer in the database
        List<Customer> customerList = customerRepository.findAll();
        assertThat(customerList).hasSize(databaseSizeBeforeUpdate);
        Customer testCustomer = customerList.get(customerList.size() - 1);
        assertThat(testCustomer.getCustFirstName()).isEqualTo(UPDATED_CUST_FIRST_NAME);
        assertThat(testCustomer.getCustLastName()).isEqualTo(UPDATED_CUST_LAST_NAME);
        assertThat(testCustomer.getCustAddress()).isEqualTo(UPDATED_CUST_ADDRESS);
        assertThat(testCustomer.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
        assertThat(testCustomer.getnLsanguage()).isEqualTo(UPDATED_N_LSANGUAGE);
        assertThat(testCustomer.getNlsTerritory()).isEqualTo(UPDATED_NLS_TERRITORY);
        assertThat(testCustomer.getCreditLimit()).isEqualTo(UPDATED_CREDIT_LIMIT);
        assertThat(testCustomer.getCustEmail()).isEqualTo(UPDATED_CUST_EMAIL);
        assertThat(testCustomer.getAccountMgrId()).isEqualTo(UPDATED_ACCOUNT_MGR_ID);
        assertThat(testCustomer.getCustGeoLocation()).isEqualTo(UPDATED_CUST_GEO_LOCATION);
        assertThat(testCustomer.getDateOfBirth()).isEqualTo(UPDATED_DATE_OF_BIRTH);
        assertThat(testCustomer.getMaritalStatus()).isEqualTo(UPDATED_MARITAL_STATUS);
        assertThat(testCustomer.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testCustomer.getIncomeLevel()).isEqualTo(UPDATED_INCOME_LEVEL);
    }

    @Test
    @Transactional
    public void updateNonExistingCustomer() throws Exception {
        int databaseSizeBeforeUpdate = customerRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCustomerMockMvc.perform(put("/api/customers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(customer)))
            .andExpect(status().isBadRequest());

        // Validate the Customer in the database
        List<Customer> customerList = customerRepository.findAll();
        assertThat(customerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCustomer() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        int databaseSizeBeforeDelete = customerRepository.findAll().size();

        // Delete the customer
        restCustomerMockMvc.perform(delete("/api/customers/{id}", customer.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Customer> customerList = customerRepository.findAll();
        assertThat(customerList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
