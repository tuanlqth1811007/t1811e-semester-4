package com.mycompany.myapp.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mycompany.myapp.web.rest.TestUtil;

public class ProductInfomationTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProductInfomation.class);
        ProductInfomation productInfomation1 = new ProductInfomation();
        productInfomation1.setId(1L);
        ProductInfomation productInfomation2 = new ProductInfomation();
        productInfomation2.setId(productInfomation1.getId());
        assertThat(productInfomation1).isEqualTo(productInfomation2);
        productInfomation2.setId(2L);
        assertThat(productInfomation1).isNotEqualTo(productInfomation2);
        productInfomation1.setId(null);
        assertThat(productInfomation1).isNotEqualTo(productInfomation2);
    }
}
