package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.Jhipster04App;
import com.mycompany.myapp.domain.WareHouse;
import com.mycompany.myapp.repository.WareHouseRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link WareHouseResource} REST controller.
 */
@SpringBootTest(classes = Jhipster04App.class)
@AutoConfigureMockMvc
@WithMockUser
public class WareHouseResourceIT {

    private static final String DEFAULT_WARE_HOUSE_SPEC = "AAAAAAAAAA";
    private static final String UPDATED_WARE_HOUSE_SPEC = "BBBBBBBBBB";

    private static final String DEFAULT_WARE_HOUSE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_WARE_HOUSE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_WH_GEO_LOCATION = "AAAAAAAAAA";
    private static final String UPDATED_WH_GEO_LOCATION = "BBBBBBBBBB";

    @Autowired
    private WareHouseRepository wareHouseRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restWareHouseMockMvc;

    private WareHouse wareHouse;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WareHouse createEntity(EntityManager em) {
        WareHouse wareHouse = new WareHouse()
            .wareHouseSpec(DEFAULT_WARE_HOUSE_SPEC)
            .wareHouseName(DEFAULT_WARE_HOUSE_NAME)
            .whGeoLocation(DEFAULT_WH_GEO_LOCATION);
        return wareHouse;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WareHouse createUpdatedEntity(EntityManager em) {
        WareHouse wareHouse = new WareHouse()
            .wareHouseSpec(UPDATED_WARE_HOUSE_SPEC)
            .wareHouseName(UPDATED_WARE_HOUSE_NAME)
            .whGeoLocation(UPDATED_WH_GEO_LOCATION);
        return wareHouse;
    }

    @BeforeEach
    public void initTest() {
        wareHouse = createEntity(em);
    }

    @Test
    @Transactional
    public void createWareHouse() throws Exception {
        int databaseSizeBeforeCreate = wareHouseRepository.findAll().size();
        // Create the WareHouse
        restWareHouseMockMvc.perform(post("/api/ware-houses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(wareHouse)))
            .andExpect(status().isCreated());

        // Validate the WareHouse in the database
        List<WareHouse> wareHouseList = wareHouseRepository.findAll();
        assertThat(wareHouseList).hasSize(databaseSizeBeforeCreate + 1);
        WareHouse testWareHouse = wareHouseList.get(wareHouseList.size() - 1);
        assertThat(testWareHouse.getWareHouseSpec()).isEqualTo(DEFAULT_WARE_HOUSE_SPEC);
        assertThat(testWareHouse.getWareHouseName()).isEqualTo(DEFAULT_WARE_HOUSE_NAME);
        assertThat(testWareHouse.getWhGeoLocation()).isEqualTo(DEFAULT_WH_GEO_LOCATION);
    }

    @Test
    @Transactional
    public void createWareHouseWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = wareHouseRepository.findAll().size();

        // Create the WareHouse with an existing ID
        wareHouse.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restWareHouseMockMvc.perform(post("/api/ware-houses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(wareHouse)))
            .andExpect(status().isBadRequest());

        // Validate the WareHouse in the database
        List<WareHouse> wareHouseList = wareHouseRepository.findAll();
        assertThat(wareHouseList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllWareHouses() throws Exception {
        // Initialize the database
        wareHouseRepository.saveAndFlush(wareHouse);

        // Get all the wareHouseList
        restWareHouseMockMvc.perform(get("/api/ware-houses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(wareHouse.getId().intValue())))
            .andExpect(jsonPath("$.[*].wareHouseSpec").value(hasItem(DEFAULT_WARE_HOUSE_SPEC)))
            .andExpect(jsonPath("$.[*].wareHouseName").value(hasItem(DEFAULT_WARE_HOUSE_NAME)))
            .andExpect(jsonPath("$.[*].whGeoLocation").value(hasItem(DEFAULT_WH_GEO_LOCATION)));
    }
    
    @Test
    @Transactional
    public void getWareHouse() throws Exception {
        // Initialize the database
        wareHouseRepository.saveAndFlush(wareHouse);

        // Get the wareHouse
        restWareHouseMockMvc.perform(get("/api/ware-houses/{id}", wareHouse.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(wareHouse.getId().intValue()))
            .andExpect(jsonPath("$.wareHouseSpec").value(DEFAULT_WARE_HOUSE_SPEC))
            .andExpect(jsonPath("$.wareHouseName").value(DEFAULT_WARE_HOUSE_NAME))
            .andExpect(jsonPath("$.whGeoLocation").value(DEFAULT_WH_GEO_LOCATION));
    }
    @Test
    @Transactional
    public void getNonExistingWareHouse() throws Exception {
        // Get the wareHouse
        restWareHouseMockMvc.perform(get("/api/ware-houses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateWareHouse() throws Exception {
        // Initialize the database
        wareHouseRepository.saveAndFlush(wareHouse);

        int databaseSizeBeforeUpdate = wareHouseRepository.findAll().size();

        // Update the wareHouse
        WareHouse updatedWareHouse = wareHouseRepository.findById(wareHouse.getId()).get();
        // Disconnect from session so that the updates on updatedWareHouse are not directly saved in db
        em.detach(updatedWareHouse);
        updatedWareHouse
            .wareHouseSpec(UPDATED_WARE_HOUSE_SPEC)
            .wareHouseName(UPDATED_WARE_HOUSE_NAME)
            .whGeoLocation(UPDATED_WH_GEO_LOCATION);

        restWareHouseMockMvc.perform(put("/api/ware-houses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedWareHouse)))
            .andExpect(status().isOk());

        // Validate the WareHouse in the database
        List<WareHouse> wareHouseList = wareHouseRepository.findAll();
        assertThat(wareHouseList).hasSize(databaseSizeBeforeUpdate);
        WareHouse testWareHouse = wareHouseList.get(wareHouseList.size() - 1);
        assertThat(testWareHouse.getWareHouseSpec()).isEqualTo(UPDATED_WARE_HOUSE_SPEC);
        assertThat(testWareHouse.getWareHouseName()).isEqualTo(UPDATED_WARE_HOUSE_NAME);
        assertThat(testWareHouse.getWhGeoLocation()).isEqualTo(UPDATED_WH_GEO_LOCATION);
    }

    @Test
    @Transactional
    public void updateNonExistingWareHouse() throws Exception {
        int databaseSizeBeforeUpdate = wareHouseRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restWareHouseMockMvc.perform(put("/api/ware-houses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(wareHouse)))
            .andExpect(status().isBadRequest());

        // Validate the WareHouse in the database
        List<WareHouse> wareHouseList = wareHouseRepository.findAll();
        assertThat(wareHouseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteWareHouse() throws Exception {
        // Initialize the database
        wareHouseRepository.saveAndFlush(wareHouse);

        int databaseSizeBeforeDelete = wareHouseRepository.findAll().size();

        // Delete the wareHouse
        restWareHouseMockMvc.perform(delete("/api/ware-houses/{id}", wareHouse.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<WareHouse> wareHouseList = wareHouseRepository.findAll();
        assertThat(wareHouseList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
