package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.Jhipster04App;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
/**
 * Test class for the EmployeeResource REST controller.
 *
 * @see EmployeeResource
 */
@SpringBootTest(classes = Jhipster04App.class)
public class EmployeeResourceIT {

    private MockMvc restMockMvc;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        EmployeeResource employeeResource = new EmployeeResource();
        restMockMvc = MockMvcBuilders
            .standaloneSetup(employeeResource)
            .build();
    }

    /**
     * Test createEmployee
     */
    @Test
    public void testCreateEmployee() throws Exception {
        restMockMvc.perform(post("/api/employee/create-employee"))
            .andExpect(status().isOk());
    }

    /**
     * Test getAllEmployee
     */
    @Test
    public void testGetAllEmployee() throws Exception {
        restMockMvc.perform(get("/api/employee/get-all-employee"))
            .andExpect(status().isOk());
    }

    /**
     * Test findEmployee
     */
    @Test
    public void testFindEmployee() throws Exception {
        restMockMvc.perform(get("/api/employee/find-employee"))
            .andExpect(status().isOk());
    }
}
