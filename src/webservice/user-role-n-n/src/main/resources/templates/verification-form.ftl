<#import "/spring.ftl" as spring />

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Activate account with SpringBoot and REST</title>
    </head>
    <body>
        <h2>Verify your email</h2>

        <@spring.bind "verificationForm" />
        <#if verificationForm ?? && noErrors??></#if>
    </body>
</html>