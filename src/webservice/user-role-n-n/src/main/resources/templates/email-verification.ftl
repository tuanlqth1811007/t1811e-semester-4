Hi customer <br/>
Thanks for using our service. Please confirm your email address by clicking on the link below. <br/>

${VERIFICATION_URL} <br/>
If you did not sign up for a T1811E account please disregard this email.<br/>

The T1811E