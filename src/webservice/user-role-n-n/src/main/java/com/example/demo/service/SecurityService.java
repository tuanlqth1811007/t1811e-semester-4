package com.example.demo.service;

import org.springframework.stereotype.Service;

public interface SecurityService  {
    String findLoggedUsername();

    void autoLogin(String username, String password);
}
