import React from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route, Link, NavLink, Redirect } from "react-router-dom";
import ListProductPage from './pages/ListProductPage';
import TodoList from "./pages/TodoList";

function App() {
  return (
    <Router>
      <Route path="/" component={ TodoList } />
      {/* <Redirect exact from="/" to="/products" />
      <Route path="/products" component={ ListProductPage } /> */}
    </Router>
  );
}

export default App;
