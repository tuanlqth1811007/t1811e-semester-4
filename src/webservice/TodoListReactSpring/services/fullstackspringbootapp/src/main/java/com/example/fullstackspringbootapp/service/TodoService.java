package com.example.fullstackspringbootapp.service;

import com.example.fullstackspringbootapp.entity.Todo;
import com.example.fullstackspringbootapp.repository.TodoRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoService {
    private final TodoRepository todoRepository;

    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public List<Todo> findAll(){
        return todoRepository.findAll();
    }

    public List<Todo> saveAll(List<Todo> todos){
        return todoRepository.saveAll(todos);
    }

    public void DeleteById(Long id){
        todoRepository.deleteById(id);
    }
}
