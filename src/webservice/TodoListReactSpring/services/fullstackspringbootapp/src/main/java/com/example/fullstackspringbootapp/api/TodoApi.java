package com.example.fullstackspringbootapp.api;

import com.example.fullstackspringbootapp.entity.Todo;
import com.example.fullstackspringbootapp.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/todos")
public class TodoApi {
    private final TodoService todoService;

    @Autowired
    public TodoApi(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    public ResponseEntity<List<Todo>> findAll(){
        return ResponseEntity.ok(todoService.findAll());
    }

    @PostMapping
    public ResponseEntity saveAll(@Valid @RequestBody List<Todo> todos){
        return ResponseEntity.ok(todoService.saveAll(todos));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Todo> deleteById(@PathVariable Long id){
        todoService.DeleteById(id);
        return ResponseEntity.ok().build();
    }

}
