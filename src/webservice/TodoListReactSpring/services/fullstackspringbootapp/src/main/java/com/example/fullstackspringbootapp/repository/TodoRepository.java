package com.example.fullstackspringbootapp.repository;

import com.example.fullstackspringbootapp.entity.Todo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TodoRepository extends JpaRepository<Todo, Long> {
    
}
