package com.example.recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<Product> productList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initData();

        ProductAdapter adapter = new ProductAdapter(this, productList);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);

        RecyclerView rvProduct = findViewById(R.id.rvProduct);

        rvProduct.setLayoutManager(layoutManager);
        rvProduct.setAdapter(adapter);
    }


    private void initData(){
        productList.add(new Product("Product 1", "500.000", R.drawable.p1));
        productList.add(new Product("Product 2", "600.000", R.drawable.p2));
        productList.add(new Product("Product 3", "700.000", R.drawable.p3));
        productList.add(new Product("Product 4", "800.000", R.drawable.p4));
        productList.add(new Product("Product 5", "900.000", R.drawable.p5));
        productList.add(new Product("Product 6", "400.000", R.drawable.p6));
    }
}