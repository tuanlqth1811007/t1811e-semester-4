package com.example.demolist;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView lvContact;
    private List<ContactModel> listContact = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();
        lvContact = (ListView) findViewById(R.id.lvContact);
        ContactAdapter adapter = new ContactAdapter(listContact, this);
        lvContact.setAdapter(adapter);

        lvContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ContactModel contactModel = listContact.get(position);
                Toast.makeText(MainActivity.this, contactModel.getName(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void initData(){
        ContactModel contactModel = new ContactModel("Nguyen van A1", "0123456789", R.drawable.user2);
        listContact.add(contactModel);
        contactModel = new ContactModel("Nguyen van A1", "0123456789", R.drawable.user1);
        listContact.add(contactModel);
        contactModel = new ContactModel("Nguyen van A2", "0123456789", R.drawable.user2);
        listContact.add(contactModel);
        contactModel = new ContactModel("Nguyen van A3", "0123456789", R.drawable.user1);
        listContact.add(contactModel);
        contactModel = new ContactModel("Nguyen van A4", "0123456789", R.drawable.user2);
        listContact.add(contactModel);
        contactModel = new ContactModel("Nguyen van A5", "0123456789", R.drawable.user1);
        listContact.add(contactModel);
        contactModel = new ContactModel("Nguyen van A6", "0123456789", R.drawable.user2);
        listContact.add(contactModel);
        contactModel = new ContactModel("Nguyen van A7", "0123456789", R.drawable.user1);
        listContact.add(contactModel);
        contactModel = new ContactModel("Nguyen van A8", "0123456789", R.drawable.user2);
        listContact.add(contactModel);
        contactModel = new ContactModel("Nguyen van A9", "0123456789", R.drawable.user1);
        listContact.add(contactModel);
        contactModel = new ContactModel("Nguyen van A10", "0123456789", R.drawable.user2);
        listContact.add(contactModel);
        contactModel = new ContactModel("Nguyen van A11", "0123456789", R.drawable.user1);
        listContact.add(contactModel);
        contactModel = new ContactModel("Nguyen van A12", "0123456789", R.drawable.user2);
        listContact.add(contactModel);
        contactModel = new ContactModel("Nguyen van A13", "0123456789", R.drawable.user1);
        listContact.add(contactModel);
    }
}