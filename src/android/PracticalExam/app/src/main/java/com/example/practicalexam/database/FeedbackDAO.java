package com.example.practicalexam.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface FeedbackDAO {
    @Insert(onConflict = REPLACE)
    void insert(FeedbackEntity fedFeedbackEntity);

    @Query("SELECT COUNT(*) FROM feedback")
    Integer getCount();
}
