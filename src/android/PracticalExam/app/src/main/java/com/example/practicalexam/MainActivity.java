package com.example.practicalexam;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.practicalexam.database.AppDatabase;
import com.example.practicalexam.database.FeedbackEntity;

public class MainActivity extends AppCompatActivity {
    private EditText edName;
    private EditText edEmail;
    private Spinner spType;
    private EditText edContent;
    private CheckBox cbMailResponse;
    private Button btSend;
    public String type;
    private TextView tvCount;
    AppDatabase db;
    private String [] dataSpiner = {"type 1", "type 2", "type 3"};

    public FeedbackEntity feedbackEntity = new FeedbackEntity();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = AppDatabase.getAppDatabase(this);
        edName = findViewById(R.id.edName);
        edEmail = findViewById(R.id.edEmail);
        spType = findViewById(R.id.spType);
        edContent = findViewById(R.id.edContent);
        cbMailResponse = findViewById(R.id.cbMailResponse);
        tvCount = findViewById(R.id.tvCount);
        btSend = findViewById(R.id.btSend);
        ArrayAdapter<String> adapter=new ArrayAdapter<String>
                (
                        this,
                        android.R.layout.simple_spinner_item,
                        dataSpiner
                );
        adapter.setDropDownViewResource
                (android.R.layout.simple_list_item_single_choice);
        spType.setAdapter(adapter);

        spType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                feedbackEntity.type = dataSpiner[position];
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }
        });

        btSend.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                feedbackEntity.name = edName.getText().toString();
                feedbackEntity.email = edEmail.getText().toString();
                feedbackEntity.content = edContent.getText().toString();
                feedbackEntity.emailResponse = cbMailResponse.isChecked();
                db.feedbackDAO().insert(feedbackEntity);
                tvCount.setText(db.feedbackDAO().getCount().toString());
            }
        });
    }
}