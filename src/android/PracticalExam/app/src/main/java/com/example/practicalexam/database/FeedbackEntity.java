package com.example.practicalexam.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "feedback")
public class FeedbackEntity {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "name")
    public String name;

    @ColumnInfo(name = "email")
    public String email;

    @ColumnInfo(name = "type")
    public String type;

    @ColumnInfo(name = "content")
    public String content;

    @ColumnInfo(name = "emailResponse")
    public Boolean emailResponse;
}
