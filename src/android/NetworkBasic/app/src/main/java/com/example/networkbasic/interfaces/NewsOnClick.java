package com.example.networkbasic.interfaces;

public interface NewsOnClick {
    void onClickItem(int position);
}
