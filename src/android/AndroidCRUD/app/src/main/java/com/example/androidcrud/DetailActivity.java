package com.example.androidcrud;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.androidcrud.model.User;

public class DetailActivity extends AppCompatActivity {
    private EditText edUsername;
    private EditText edPassword;
    private EditText edEmail;
    private EditText edFullName;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        User user = (User) getIntent().getSerializableExtra("user");
        edUsername = findViewById(R.id.edUsername);
        edPassword = findViewById(R.id.edPassword);
        edEmail = findViewById(R.id.edEmail);
        edFullName = findViewById(R.id.edFullName);

        edUsername.setText(user.getUsername());
        edPassword.setText(user.getPassword());
        edEmail.setText(user.getEmail());
        edFullName.setText(user.getFullName());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
