package com.example.androidcrud.network;

import com.example.androidcrud.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface APIManager {

    @GET("user")
    Call<List<User>> getUsers();
}
