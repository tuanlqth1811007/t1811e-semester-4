package com.example.androidcrud;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.androidcrud.adapter.UserAdapter;
import com.example.androidcrud.model.User;
import com.example.androidcrud.network.APIManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    private static final String SERVER_URL = "https://5fc876712af77700165ad58f.mockapi.io/api/";
    private ListView lvUser;
    private ArrayList<User> users = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lvUser = (ListView) findViewById(R.id.lvUser);
        //Lay du lieu
        getData();

    }


    private void getData(){
        try {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(SERVER_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
            Log.d("Loggggg", "Before request");
            APIManager service = retrofit.create(APIManager.class);

            service
                    .getUsers()
                    .enqueue(new Callback<List<User>>() {
                        @Override
                        public void onResponse(Call<List<User>> call, Response<List<User>> response) {

                            if(response.isSuccessful()){
                                System.out.println("12312312312");
                            }
                            Log.d("Loggggg", response.body().toString());
                            for(User u: response.body()){
                                System.out.println(u.getEmail());
                            }
                            System.out.println(response.body().toString() + "letuan");
                            if(response.body() == null){
                                return;
                            }

                            users = (ArrayList<User>) response.body();
                            System.out.println("le tuan");
//                            for (int i = 0; i< 100; i++){
//                                users.add(new User(i ,"User" + i, "password" + i, "fullname" + i, "email" + i));
//                            }
                            lvUser = findViewById(R.id.lvUser);

                            //Tao adapter
                            UserAdapter userAdapter = new UserAdapter(getApplicationContext(), R.layout.item_user, users);
                            lvUser.setAdapter(userAdapter);

                            lvUser.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
                                    intent.putExtra("user", users.get(position));
                                    startActivity(intent);
                                }
                            });
                        }

                        @Override
                        public void onFailure(Call<List<User>> call, Throwable t) {
                            Log.d("Loggggg", "onFailure");
                        }
                    });
        }catch (Exception e){
            System.out.println("Error " + e);
        }
    }

}