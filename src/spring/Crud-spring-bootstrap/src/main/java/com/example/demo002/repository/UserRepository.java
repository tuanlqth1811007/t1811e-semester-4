package com.example.demo002.repository;

import com.example.demo002.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {
    //Difference business method
    @Query(value = "SELECT u FROM User u where u.name like %:keyword%") //Query của jpa like
    public List<User> search(String keyword);
}
