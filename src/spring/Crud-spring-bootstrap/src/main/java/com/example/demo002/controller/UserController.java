package com.example.demo002.controller;

import com.example.demo002.entity.User;
import com.example.demo002.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class UserController {
    //DI call model, step 2
    @Autowired
    UserRepository userRepository;

    @RequestMapping("/")
    public String index(Model model) {
        List<User> users = new ArrayList<>();
        users = (List<User>) userRepository.findAll();
        model.addAttribute("users", users);
        return "index";
    }

    @RequestMapping("/add")
    public String add(Model model) {
        model.addAttribute("user", new User());
        return "add-user";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String addUser(@ModelAttribute User user, Model model) {
        userRepository.save(user);
        return "redirect:/";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String editUser( @RequestParam("id") Long userId,Model model) {
        Optional<User> userEdit = userRepository.findById(userId);
        userEdit.ifPresent(user -> model.addAttribute("user", user));
        return "edit-user";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    @ResponseBody
    public String deleteUser( @RequestParam("id") Long userId,Model model) {
        userRepository.deleteById(userId);
        return "{success: true}";
    }
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String searchUser(@RequestParam("name") String name, Model model){
        List<User> users = new ArrayList<>();
        users = (List<User>) userRepository.search(name);
        model.addAttribute("users", users);
        return "index";
    }
}
