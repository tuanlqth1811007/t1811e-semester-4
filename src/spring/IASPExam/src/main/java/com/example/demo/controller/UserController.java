package com.example.demo.controller;

import com.example.demo.entity.User;
import com.example.demo.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@Controller
public class UserController {
    @Autowired
    private UserModel userModel;

    @RequestMapping(path = "/users", method = RequestMethod.GET)
    public String getUsers(Model model, @ModelAttribute User user) {
        List<User> users = (List<User>) userModel.findAll();
        model.addAttribute("users", users);
        model.addAttribute("message", null);
        model.addAttribute("action", "/user/save");
        return "user-home";
    }

    @RequestMapping(path = "/user/save", method = RequestMethod.POST)
    public String saveUser(Model model, @ModelAttribute User user) {
        try {
            userModel.save(user);
            return "redirect:/users";
        } catch (Exception e) {
            return "not-found";
        }
    }

    @RequestMapping(path = "/user/edit/{id}", method = RequestMethod.GET)
    public String editUser(Model model, @PathVariable int id){
        Optional<User> user = userModel.findById(id);
        if (user.isPresent()) {
            List<User> users = (List<User>) userModel.findAll();
            model.addAttribute("action", "/user/save");
            model.addAttribute("user", user);
            model.addAttribute("users", users);
            model.addAttribute("message", "Edit form");
            return "user-home";
        } else {
            return "not-found";
        }
    }

    @RequestMapping(path = "/user/delete/{id}", method = RequestMethod.POST)
    public String deleteUser(Model model, @PathVariable int id){
        userModel.deleteById(id);
        model.addAttribute("message", "Delete Success");
        return "redirect:/users";
    }

    @RequestMapping(path = "/user/search", method = RequestMethod.GET)
    public String deleteUser(Model model, @RequestParam String name, @ModelAttribute User user){
        List<User> users =  (List<User>) userModel.findAllByNameContains(name);
        model.addAttribute("users", users);
        model.addAttribute("action", "/user/save");
        model.addAttribute("message", "Search success");
        return "user-home";
    }
}
