package com.example.demo.model;

import com.example.demo.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserModel extends CrudRepository<User, Integer> {

    List<User> findAllByNameContains(String name);
}
