package com.example.onetomany.repository;

import com.example.onetomany.entity.BookCategory;
import org.springframework.data.repository.CrudRepository;

public interface BookCategoryRepository extends CrudRepository<BookCategory, Integer> {
}
