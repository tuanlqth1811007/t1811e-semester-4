package com.example.onetomany.repository;

import com.example.onetomany.entity.Book;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;//ACID

import java.util.List;

@Transactional(readOnly = true)
public interface BookRepository extends CrudRepository<Book, Integer> {
    @EntityGraph(attributePaths = "bookCategory")
    List<Book> findFirstByOrderByNameAsc();

    @Modifying
    @Transactional
    @Query("delete from Book b where b.bookCategory.Id = ?1")
    void deleteByBookCategoryId(int categoryId);
}
