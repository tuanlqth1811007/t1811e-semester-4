package com.example.onetomany;

import com.example.onetomany.service.BookService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnetomanyApplication implements CommandLineRunner {
    private final BookService bookService;

    public OnetomanyApplication(BookService bookService) {
        this.bookService = bookService;
    }

    public static void main(String[] args) {
        SpringApplication.run(OnetomanyApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        bookService.initData();
        bookService.read();
        bookService.delete();
    }
}
