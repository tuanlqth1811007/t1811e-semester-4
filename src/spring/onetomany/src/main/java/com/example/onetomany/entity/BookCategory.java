package com.example.onetomany.entity;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "BookCategory")
@Table(name = "book_category")
public class BookCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int Id;

    public BookCategory(String name) {
        this.name = name;
        books = new HashSet<>();
    }

    @Column(name = "category_name")
    private String name;
    @OneToMany(
            mappedBy = "bookCategory",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    private Set<Book> books;

    public BookCategory(int id, String name) {
        Id = id;
        this.name = name;
    }

    public BookCategory() {
        books = new HashSet<>();
    }


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Book> getBook(){
        return books;
    }

    public void setBooks(Set<Book> books){
        this.books = books;
        for (Book book: books){
            book.setBookCategory(this);
        }
    }

}
