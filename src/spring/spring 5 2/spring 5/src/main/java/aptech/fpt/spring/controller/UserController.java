package aptech.fpt.spring.controller;

import aptech.fpt.spring.entity.User;
import aptech.fpt.spring.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.Optional;

@Controller
public class UserController {
    @Autowired
    private UserModel userModel;

    @RequestMapping(path = "/user/list", method = RequestMethod.GET)
    public String getUsers(Model model, @RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "10") int limit) {
        Page<User> pagination = userModel.findAll(PageRequest.of(page - 1, limit));
        model.addAttribute("pagination", pagination);
        model.addAttribute("page", page);
        model.addAttribute("limit", limit);
        model.addAttribute("datetime", Calendar.getInstance().getTime());
        return "user-list";
    }

    @RequestMapping(path = "/user/create", method = RequestMethod.GET)
    public String createUser(Model model, @ModelAttribute User user) {
        model.addAttribute("action", "/user/save");
        return "user-form";
    }

    @RequestMapping(path = "/user/edit/{id}", method = RequestMethod.GET)
    public String editUser(Model model, @PathVariable int id) {
        Optional<User> user = userModel.findById(id);
        if (user.isPresent()) {
            model.addAttribute("action", "/user/save");
            model.addAttribute("user", user);
            return "user-form";
        } else {
            return "not-found";
        }
    }

    @RequestMapping(path = "/user/save", method = RequestMethod.POST)
    public String saveUser(Model model, @ModelAttribute User user) {
        try {
            userModel.save(user);
            return "redirect:/user/list";
        } catch (Exception e) {
            return "not-found";
        }
    }

    @RequestMapping(path = "/user/delete/{id}", method = RequestMethod.POST)
    public String deleteUser(Model model, @PathVariable int id) {
        Optional<User> optionalUser = userModel.findById(id);
        if (optionalUser.isPresent()) {
            userModel.delete(optionalUser.get());
            return "redirect:/user/list";
        } else {
            return "not-found";
        }
    }

    @RequestMapping(path = "/user/search", method = RequestMethod.GET)
    public String searchUser(Model model, @RequestParam(defaultValue = "") String name, @RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "10") int limit) {
        Page<User> pagination = userModel.findAllByNameContains(name, PageRequest.of(page - 1, limit));
        model.addAttribute("pagination", pagination);
        model.addAttribute("page", page);
        model.addAttribute("limit", limit);
        model.addAttribute("datetime", Calendar.getInstance().getTime());
        return "user-list";
    }
}
