<%--
  Created by IntelliJ IDEA.
  User: letua
  Date: 9/7/20
  Time: 11:03 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>User Manager Application</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">
    </head>
    <body>
        <header>
            <nav class="navbar navbar-expand-md navbar-dark">
                <div>
                    <a href="" class="navbar-brand">User management app</a>
                </div>
                <ul class="navbar-nav">
                    <li><a href="<%=request.getContextPath()%>/list" class="nav-link">Users</a></li>
                </ul>
            </nav>
        </header>
        <br/>
        <br/>
        <div class="container col-md-5">
            <div class="card">
                <div class="card-body">
                    <c:set var="user" value="${requestScope.user}" scope="request"/>
                    <c:if test="${user != null}">
                    <form action="update" method="post">
                        </c:if>
                        <c:if test="${user == null}">
                        <form action="insert" method="post">
                            </c:if>
                            <caption>
                                <h2>
                                    <c:if test="${user != null}">
                                        Edit user
                                    </c:if>
                                    <c:if test="${user == null}">
                                        Add new user
                                    </c:if>
                                </h2>
                            </caption>
                                <c:if test="${user != null}">
                                   <input type="hidden" name="id" value="<c:out value='${user.id}'/>">
                                </c:if>
                            <fieldset class="form-group">
                                <label>Username</label>
                                <input type="text" value="<c:out value='${user.name}'/>" class="form-control"
                                       name="name" required="required"/>
                            </fieldset>
                                <fieldset class="form-group">
                                    <label>Email</label>
                                    <input type="text" value="<c:out value='${user.email}'/>" class="form-control"
                                           name="email"/>
                                </fieldset>
                                </fieldset>
                                <fieldset class="form-group">
                                    <label>Country</label>
                                    <input type="text" value="<c:out value='${user.country}'/>" class="form-control"
                                           name="country"/>
                                </fieldset>
                            <button type="submit" class="btn btn-success">Save</button>
                        </form>
                </div>
            </div>
        </div>
    </body>
</html>
