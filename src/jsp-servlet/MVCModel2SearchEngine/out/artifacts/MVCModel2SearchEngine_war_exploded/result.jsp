<%--
  Created by IntelliJ IDEA.
  User: letua
  Date: 8/28/20
  Time: 7:59 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>List product</title>
    </head>
    <body>
        <h1>List product</h1>
        <a href="search.jsp">Search</a>
        <jsp:useBean id="finder" scope="request" class="com.wpsj.model.ProductFinderBean" />
        <table>
            <tr>
                <td>Id</td>
                <td>Name</td>
                <td>Description</td>
            </tr>
            <c:forEach items="${finder.searchProducts}" var="product">
            <tr>
                <td>${product.id}</td>
                <td>${product.name}</td>
                <td>${product.desc}</td>
            </tr>
            </c:forEach>
        </table>
    </body>
</html>
