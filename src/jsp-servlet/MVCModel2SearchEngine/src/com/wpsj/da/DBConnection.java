package com.wpsj.da;

import java.sql.*;
import java.util.ArrayList;

public class DBConnection {
    private static Connection connection = null;

    private static PreparedStatement _preparedStatement = null;

    public static Connection createConnection() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/t1811e-java", "root", "86516562");
        return connection;
    }

    public ResultSet executeStatement(String preparedStatement, ArrayList listParam) throws SQLException, ClassNotFoundException {
        createConnection();
        _preparedStatement = connection.prepareStatement(preparedStatement);
        int i = 1;
        System.out.println(listParam.get(i - 1).toString() + "tuan 1999");
        for (Object param: listParam) {
            _preparedStatement.setString(i, param.toString());
        }
        System.out.println(_preparedStatement.toString());
        ResultSet resultSet = _preparedStatement.executeQuery();
        return resultSet;
    }
}
