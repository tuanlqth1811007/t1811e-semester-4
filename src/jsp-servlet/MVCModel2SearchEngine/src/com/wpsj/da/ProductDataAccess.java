package com.wpsj.da;

import com.wpsj.entity.Product;

import java.sql.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductDataAccess {
    private final static DBConnection dbConnection = new DBConnection();
    private static PreparedStatement preparedStatement = null;

    public ArrayList<Product> getProduct() throws SQLException, ClassNotFoundException {
        String statement = "select * from product";

        ArrayList<Product> products = new ArrayList<Product>();
        ResultSet result = dbConnection.executeStatement(statement, null);
        while (result.next()){
            Product product = new Product();
            product.setName(result.getString("name"));
            product.setDesc(result.getString("desc"));
            System.out.println(product.getName());
            products.add(product);
        }
        return products;
    }

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        ProductDataAccess productDataAccess = new ProductDataAccess();
        productDataAccess.getProduct();
    }

    public ArrayList<Product> searchProduct(String name) throws SQLException, ClassNotFoundException {
        String query = "select * from product where name LIKE ?";
        ArrayList listParamQuery = new ArrayList();
        listParamQuery.add("%"+ name + "%");
        ArrayList<Product> products = new ArrayList<Product>();
        ResultSet result = dbConnection.executeStatement(query, listParamQuery);
        while (result.next()){
            Product product = new Product();
            product.setName(result.getString("name"));
            product.setDesc(result.getString("desc"));
            products.add(product);
        }
        return products;
    }
}
