package com.wpsj.model;

import com.wpsj.da.ProductDataAccess;
import com.wpsj.entity.Product;

import java.sql.SQLException;
import java.util.List;

public class ProductFinderBean {
    private String _searchString;

    public void setSearchString(String searchString){
        this._searchString = searchString;
    }

    public List<Product> getSearchProducts() throws SQLException, ClassNotFoundException {
        ProductDataAccess productDataAccess = new ProductDataAccess();
        return productDataAccess.searchProduct(this._searchString);
    }
}
