package com.wpsj.entity;

public class Product {
    public int Id;
    public String Name;
    public String Desc;

    public Product(int id, String name, String desc) {
        Id = id;
        Name = name;
        Desc = desc;
    }

    public Product() {
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String desc) {
        Desc = desc;
    }
}
