package com.wpsj.controller;

import com.wpsj.model.ProductFinderBean;

import javax.servlet.RequestDispatcher;
import java.io.IOException;
import java.sql.SQLException;

public class ProductFinder extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        String searchString = request.getParameter("name");
        ProductFinderBean productFinderBean = new ProductFinderBean();
        if(searchString != null && searchString.trim().isEmpty()){
            response.sendRedirect("search.jsp?mgs= Enter keyword please !!!");
        }
        System.out.println(searchString + " tuan");
        productFinderBean.setSearchString(searchString);
        request.setAttribute("finder", productFinderBean);

        RequestDispatcher rd  = request.getRequestDispatcher("result.jsp");
        rd.forward(request,response);

    }
}
