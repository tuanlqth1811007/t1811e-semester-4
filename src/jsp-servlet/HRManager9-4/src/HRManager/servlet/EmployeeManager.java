/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package HRManager.servlet;

import HRManager.bol.EmployeeBO;
import HRManager.entities.Employee;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class EmployeeManager extends HttpServlet {
    HRManager.entities.Employee[] arr=null;
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session.getAttribute("username") == null) {
            response.sendRedirect("login.jsp");
        }
        HRManager.bol.EmployeeBO ebo = new HRManager.bol.EmployeeBO();
        String value = request.getParameter("txtValue");
        String option = request.getParameter("ddlSearch");
        if (value == null || option == null) {
            RequestDispatcher reqdis = request.getRequestDispatcher("employeeManager.jsp");
            EmployeeBO employeeBO = new EmployeeBO();
            arr = employeeBO.select();
            session.setAttribute("Empolyees", arr);
            reqdis.forward(request, response);
        } else {
            RequestDispatcher reqdis = request.getRequestDispatcher("employeeManager.jsp?option=" + option + "&value=" + value);
            EmployeeBO employeeBO = new EmployeeBO();
            if (option.equals("Name")) {
                arr = employeeBO.find(0, value);
            } else if (option.equals("City")) {
                arr = employeeBO.find(1, value);
            }
            session.setAttribute("Empolyees", arr);
            reqdis.forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}