<%--
  Created by IntelliJ IDEA.
  User: letua
  Date: 9/10/20
  Time: 7:43 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
    <head>
        <title>Title</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    </head>
    <body>
        <table class="table table-bordered">
            <thead>
                <td>Id</td>
                <td>fullname</td>
                <td>birthday</td>
                <td>address</td>
                <td>position</td>
                <td>department</td>
            </thead>
            <c:set var="listEmp" value="${requestScope.listEmp}" scope="request"/>
            <tbody>
                <c:forEach var="emp" items="${listEmp}">
                    <tr>
                        <td>
                            <c:out value="${emp.id}"/>
                        </td>
                        <td>
                            <c:out value="${emp.fullname}"/>
                        </td>
                        <td>
                            <c:out value="${emp.birthday}"/>
                        </td>
                        <td>
                            <c:out value="${emp.address}"/>
                        </td>
                        <td>
                            <c:out value="${emp.position}"/>
                        </td>
                        <td>
                            <c:out value="${emp.department}"/>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </body>
</html>
