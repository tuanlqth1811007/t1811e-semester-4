import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DAO {
    private String jdbcUrl = "jdbc:mysql://localhost:3306/t1811e?useSSL=false";
    private String jdbcUsername = "root";
    private String jdbcPassword = "86516562";

    protected Connection getConnection() throws ClassNotFoundException, SQLException {
        Connection connection = null;
        Class.forName("com.mysql.cj.jdbc.Driver");
        connection = DriverManager.getConnection(jdbcUrl, jdbcUsername, jdbcPassword);
        return connection;
    }



    public List<EmployeeModel> getAllEmployee() throws SQLException, ClassNotFoundException {
        List<EmployeeModel> list =new ArrayList<>();
        Connection connection = getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("select * from employee");
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()){
            int id = rs.getInt("id");
            String fullname = rs.getString("fullname");
            String birthday = rs.getString("birthday");
            String address = rs.getString("address");
            String position = rs.getString("position");
            String department = rs.getString("department");
            list.add(new EmployeeModel(id, fullname, birthday, address, position, department));
        }
        return list;
    }

    public void insertEmployee(EmployeeModel employeeModel) throws SQLException, ClassNotFoundException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO employee (fullname, birthday, address, position, department) VALUES (?, ?, ?, ?, ?);");
        preparedStatement.setString(1, employeeModel.getFullname());
        preparedStatement.setString(2,  employeeModel.getBirthday());
        preparedStatement.setString(3, employeeModel.getAddress());
        preparedStatement.setString(4, employeeModel.getPosition());
        preparedStatement.setString(5, employeeModel.getDepartment());
        preparedStatement.executeUpdate();
    }
}
