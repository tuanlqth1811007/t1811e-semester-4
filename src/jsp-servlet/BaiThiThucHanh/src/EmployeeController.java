import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@WebServlet("/")
public class EmployeeController extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        String action = request.getServletPath();
        DAO dao = new DAO();
        try {
            switch (action) {
                case "/reset":
                    break;
                case "/add-form":
                    RequestDispatcher dispatcher2 = request.getRequestDispatcher("addEmployee.jsp");
                    dispatcher2.forward(request, response);
                    break;
                case "/add":
                    DateFormat df = new SimpleDateFormat("yyyy/mm/dd");
                    String fullname = request.getParameter("fullname");
                    String birthday = request.getParameter("birthday");
                    String address = request.getParameter("address");
                    String position = request.getParameter("position");
                    String department = request.getParameter("department");
                    EmployeeModel employeeModel = new EmployeeModel(fullname, birthday, address, position, department);
                    dao.insertEmployee(employeeModel);
                    RequestDispatcher dispatcher0 = request.getRequestDispatcher("employee.jsp");
                    List<EmployeeModel> listEmp0 = dao.getAllEmployee();
                    request.setAttribute("listEmp", listEmp0);
                    dispatcher0.forward(request, response);
                    break;
                case "/list":
                    List<EmployeeModel> listEmp1 = dao.getAllEmployee();
                    RequestDispatcher dispatcher4 = request.getRequestDispatcher("employee.jsp");
                    request.setAttribute("listEmp", listEmp1);
                    dispatcher4.forward(request, response);
                    break;
                default:
                    List<EmployeeModel> listEmp = dao.getAllEmployee();
                    RequestDispatcher dispatcher = request.getRequestDispatcher("employee.jsp");
                    request.setAttribute("listEmp", listEmp);
                    dispatcher.forward(request, response);
                    break;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            throw new ServletException(ex);
        }
    }
}

